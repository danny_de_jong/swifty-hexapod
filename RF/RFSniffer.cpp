

#include "RCSwitch.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>

RCSwitch mySwitch;c

using namespace std;

int main(int argc, char *argv[]) {
	// cout << "Started my self" << endl;
     // This pin is not the first pin on the RPi GPIO header!
     // Consult https://projects.drogon.net/raspberry-pi/wiringpi/pins/
     // for more information.
     int PIN = 29;
     int xVal = 0;
     int yVal = 0;
     if(wiringPiSetup() == -1)
       return 0;

     mySwitch = RCSwitch();
     mySwitch.enableReceive(PIN);  // Receiver on inerrupt 0 => that is pin #2


     while(1) {

      if (mySwitch.available()) {
        //int value = mySwitch.getReceivedValue();
		printf("%d\n", mySwitch.getReceivedValue());

        mySwitch.resetAvailable();

      }

  }

  exit(0);


}

