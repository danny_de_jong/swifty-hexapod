import RPi.GPIO as GPIO, time

GPIO.setmode(GPIO.BCM)

def RCtime (pipin) :
	
	measurement = 0
	GPIO.setup(pipin, GPIO.OUT)
	GPIO.output(pipin, GPIO.LOW)
	time.sleep(0.1)
	
	GPIO.setup(pipin, GPIO.IN)
	while (GPIO.input(pipin) == GPIO.LOW) :
	   measurement += 1
	   

	return measurement
	

while True:
	print RCtime(21)