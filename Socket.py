__author__ = 'Umit Aksu'

import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#Binding socket
server_address = ('0.0.0.0',10000)
print("Starting up %s on %s port",server_address,sock.bind(server_address))


# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print >>sys.stderr, 'waiting for a connection'
    connection, client_address = sock.accept()
    try:
        print >>sys.stderr, 'connection from', client_address
        connection.sendall("/ready")
        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(256)
            print >>sys.stderr, 'received "%s"' % data
            if data:
                data.strip() # Strip all the trailings
                for item in data.split(':'):
                    print item

                if data.strip() == "getTemp(11)":
                    connection.sendall(str(servos.readTemperature(11)))
                    print "rev"
                if data.strip() == "move(11)":
                    servos.move(11,256) 
                if data.strip() == "exit":
                    connection.close()
                    exit(1)
            else:
                print >>sys.stderr, 'no more data from', client_address
                break

    finally:
        # Clean up the connection
        connection.close()

