from AX12 import Ax12
from ax12sender import Ax12sender
import time
import math

x = Ax12sender()
servo = Ax12()

def shootPen():
	x.addMovement(33,200,1000)
	x.sendData()
	x.addMovement(32,150,1000)
	x.addMovement(33,0,1000)
	x.sendData()

	x.addMovement(23,800,100)
	x.sendData()	
	x.addMovement(22,750,100)
	x.addMovement(23,300,100)
	x.sendData()

def initPosition():
        x.addMovement(1,613,150)
        x.addMovement(11,410,150)
        x.addMovement(21,512,150)
	x.addMovement(31,512,150)
	x.addMovement(41,410,150)
	x.addMovement(51,613,150)
	x.addMovement(2,712,150)
	x.addMovement(3,670,150)
	
	
	#x.move(11,410)
	x.addMovement(12,312,150)
	x.addMovement(13,355,150)
	
	
	x.addMovement(21,512,150)
	x.addMovement(22,712,150)
	x.addMovement(23,670,150)
	
	#x.move(31,512)
	x.addMovement(32,312,150)
	x.addMovement(33,355,150)

	
	#x.move(41,410)
	x.addMovement(42,712,150)
	x.addMovement(43,670,150)
	
	#x.move(51,613)
	x.addMovement(52,312,150)
	x.addMovement(53,355,150)
	x.sendData()

def dance():
	while(True):
		x.addMovement(1,305,150)
	        x.addMovement(11,205,150)
	        x.addMovement(21,256,150)
		x.addMovement(31,256,150)
		x.addMovement(41,205,150)
		x.addMovement(51,307,150)
		time.sleep(0.5)
		x.addMovement(2,356,150)
		time.sleep(0.5)
		x.addMovement(3,335,150)
		time.sleep(0.5)
		
		#x.move(11,410)
		time.sleep(0.5)
		x.addMovement(12,156,150)
		time.sleep(0.5)
		x.addMovement(13,178,150)
		time.sleep(0.5)
		
		x.addMovement(21,256,150)
		time.sleep(0.5)
		x.addMovement(22,356,150)
		time.sleep(0.5)
		x.addMovement(23,335,150)
		time.sleep(0.5)
		
		#x.move(31,512)
		time.sleep(0.5)
		x.addMovement(32,156,150)
		time.sleep(0.5)
		x.addMovement(33,178,150)
		time.sleep(0.5)
		
		#x.move(41,410)
		time.sleep(0.5)
		x.addMovement(42,356,150)
		time.sleep(0.5)
		x.addMovement(43,335,150)
		
		#x.move(51,613)
		time.sleep(0.5)
		x.addMovement(52,156,150)
		time.sleep(0.5)
		x.addMovement(53,156,150)
		time.sleep(0.5)
	
		x.sendData()
		
		initPosition()

alphas = [512, 512, 512, 512, 512] #lijst met de 6 nieuwe waarden voor de primaire servos
betas = [512,512,512,512,512,512]#lijst met de 6 nieuwe waarden voor de secundaire servos
gammas = [512,512,512,512,512,512] #lijst met de 6 nieuwe waardem voor de tertiare servos
alphas0 = [613,409,512,512,409,613] #lijst met de 6 midden-waardes voor de primaire servos
#epsilon = 1
def kinematics(pootnr, alpha, height):
	w = 150
	a = 50
 	b = 82.5
	c = 137
	h = height #50

	alphas[pootnr] = float(alpha)#servo.readPosition(1)
	alpha1 = float((float(alphas[pootnr]) - alphas0[pootnr]) / 195)
	s = w / math.cos(alpha1) - a
	d = math.sqrt((s*s) + (h*h))
	epsilon = math.atan(h/d)
	betas[pootnr] = math.acos(((c*c) - (b*b) - (d*d)) / (-2 * b * d))
	gammas[pootnr] = math.acos(((d*d)-(b*b)-(c*c)) / (-2 * b * c))
	
	
	#alpha40 = float(alpha[1])#servo.readPosition(1)
        #alpha401 = float((float(alpha40) - 409) / 195)
        #s40 = w0 / math.cos(alpha401) - a
        #d40 = math.sqrt((s40*s40) + (h*h))
        #epsilon40 = math.atan(h/d40)
        #beta40 = math.acos(((c*c) - (b*b) - (d40*d40)) / (-2 * b * d40))
        #gamma40 = math.acos(((d40*d40)-(b*b)-(c*c)) / (-2 * b * c))


	#print alpha00
	#print int(round((512-195*epsilon00 + 195 * beta00)))
	#print int(round((468+195*gamma00)))

	
	print "============= POOTNR: ============="
        print str(alphas[pootnr])
        print alpha1
        print s
        print d
        print epsilon
        print betas[pootnr]
        print gammas[pootnr]

#def makeMovement(pootnr):
	#for i in range(0, pootnr+1):
	if(pootnr%2==0):
		x.addMovement(pootnr*10 +1,int(alphas[pootnr]),600)
		x.addMovement(pootnr*10 +2,int(round((512-195*epsilon + 195 * betas[pootnr]))), 600)
		x.addMovement(pootnr*10 +3,int(round((468+195*gammas[pootnr]))), 600)
	else:
		x.addMovement(pootnr*10 +1,int(alphas[pootnr]),600)
                x.addMovement(pootnr*10 +2,int(round((512+195*epsilon - 195 * betas[pootnr]))), 600)
                x.addMovement(pootnr*10 +3,int(round((468-195*gammas[pootnr]))), 600)
	#x.addMovement(41,alpha,300)
        #x.addMovement(42,int(round((512-195*epsilon40 + 195 * beta40))), 300)
        #x.addMovement(43,int(round((468+195*gamma40))), 300)


	x.sendData()
	alphas[pootnr] = float(alpha)#servo.readPosition(1)
        alpha1 = float((float(alphas[pootnr]) - alphas0[pootnr]) / 195)
        s = w / math.cos(alpha1) - a
        d = math.sqrt((s*s) + (h*h))
        epsilon = math.atan(h/d)
        betas[pootnr] = math.acos(((c*c) - (b*b) - (d*d)) / (-2 * b * d))
        gammas[pootnr] = math.acos(((d*d)-(b*b)-(c*c)) / (-2 * b * c))
	

front = [511,512,409,614,307,716]# voorste waarden van de servos
back = [716,307,614,409,512,511]# achterste waarden van de servos
height = 50
forward = False
input = [511,512,512,512,512,512]

#kinematics(2,512, 50)




initPosition()
time.sleep(2)

#while(True):
#	kinematics(511)
#	time.sleep(5)
#	kinematics(716)
#
#	time.sleep(5)
i = 0
while(True):#voor poot 0 en 4
#	while(i<6):
	kinematics(i, input[i], height)
#	makeMovement(0)
	time.sleep(0.05)
	if(input <= back[i] and not forward):
		input[i] += 2
		print str(forward)
	elif(input >= back[i] and not forward):
		forward = True
		height = 10
		print str(forward)
	elif(input >= front[i] and forward):
		input[i] -= 2
		print "--"
	elif(input <= front[i]):
		forward = False
		height = 50
		str(forward)
#	i += 1


#initPosition()
#raw_input("Target locked?")
#shootPen()
