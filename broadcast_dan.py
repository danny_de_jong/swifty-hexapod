from ax12 import Ax12
from ax12sender import Ax12sender

import time
import math
#from kinematica import Kinematica

x = Ax12sender()

servo = Ax12()


def midPosition():
	global speed
	placeholder = speed
	speed = 150
	for i in range(0,6):
			calculate(i, mid[i], heightDown)
	speed = placeholder
	moveMultiple()
	x.sendData()

def initPosition(): 
	global speed
	placeholder = speed
	speed = 150
	for i in range(0,6):
		if(i in tripod['A']):
			calculate(i, backward[i], heightUp)
		elif(i in tripod['B']):
                        calculate(i, forward[i], heightDown)
	speed = placeholder
	moveMultiple()
        x.sendData()

#-------------------------------#
#   KINEMATICA			#
#-------------------------------#
	
#forward = [511, 512, 409, 614, 307, 716] #Forward limit per poot (difference 205)
#backward = [716, 307, 614, 409, 512, 511] #Backward limit per poot (difference 205)
#forward = [526, 497, 424, 599, 322, 701] #Forward limit per poot (difference 175)
#backward = [701, 322, 599, 424, 497, 526] #Backward limit per poot (difference 175)
forward = [540, 483, 438, 585, 336, 687] #Forward limit per poot (difference 146)
backward = [686, 337, 584, 439, 482, 541] #Backward limit per poot (difference 146)

mid = [613,409,512,512,409,613]
difference = 146	#Grootte van de loopbeweging, gemeten in servo-orientatie-waardes (1024 = 300 graden)
stepSize = 2		#Hoe kleiner, hoe rechter. Hoe groter, hoe dichter bij de opgegeven 'speed'
#als difference%stepSize niet 0 is, schiet het pootje in de loopbeweging iets uit!

def calcDifference(): #Bereken het gemiddelde verschil tussen de forward en backward limits van elke poot
		global difference
		total = 0
		for i in range(0,6):
			if(i%2==0):
					total += backward[i] - forward[i]
			else:
					total -= forward[i] - backward[i] 
		difference = total / 6
tripod = {}
tripod['A'] = [0, 3, 4] #Pootnummers van de tripods (MOET GETEST WORDEN)
tripod['B'] = [1, 2, 5] #Als dit array-gebruik problemen geeft, dan alle occurences weer terug-veranderen naar 'tripodA' en 'tripodB'
leftLegs= [0,2,4]
rightLegs= [1,3,5]

alphas = [613,409,512,512,409,613]
betas = [0,0,0,0,0,0]
gammas = [0,0,0,0,0,0]

heightUp = 50	#Standaard-hoogte voor de loopbeweging (Poot omhoog)
heightDown = 70	#Standaard-hoogte voor de loopbeweging (Poot omlaag)
w = 120 #standaard w

legHeight = [heightDown,heightDown,heightDown,heightDown,heightDown,heightDown] #hoogte per poot. Standaardwaarde (Voor de eerste 'calculate') is 'heightDown'
legWidth = [w,w,w,w,w,w] #afstand tot servo alpha per poot. Standaardwaarde is w

speed = 600



def calculate(pootnr, alpha, height):
        global w # Afstand tot body
	global h # Hoogte ten opzichte van servo alpha
        a = 50	
        b = 82.5
        c = 137
        h = height 

	global legHeight
	global legWidth		
        legHeight[pootnr] = height
	legWidth[pootnr] = w

        global alphas
        global betas
        global gammas
        
        #1,2,3
        alpha00 = alpha
        alpha001 = float((float(alpha00) - mid[pootnr]) / 195)
        s00 = w / math.cos(alpha001) - a
        d00 = math.sqrt((s00*s00) + (h*h))
        epsilon00 = math.atan(h/d00)
        beta = math.acos(((c*c) - (b*b) - (d00*d00)) / (-2 * b * d00))
        gamma = math.acos(((d00*d00)-(b*b)-(c*c)) / (-2 * b * c))
        
        # De poten die tegelijk bewegen
        if(pootnr%2 ==0):
                alphas[pootnr] = alpha
                betas[pootnr] = int(round((512-195 * epsilon00+195*beta)))
                gammas[pootnr] = int(round((468+195 * gamma)))
                
        else:
                alphas[pootnr] = alpha
                betas[pootnr] = int(round((512+195 * epsilon00-195*beta)))
                gammas[pootnr] = int(round((555-195 * gamma)))


#def calculate(pootnr, alpha, height):
#        kinematica(pootnr, alpha, height)
#        global alphas = getAlphas
#        global betas = getBetas
#        global gammas = getGammas


def forwardsMovement(pootnr):
        calculate(pootnr, forward[pootnr], heightUp)
        

def calcLeg(pootnr, count, goingForward):
        #Even poten gaan met + , oneven met - naar achter
        #Even poten gaan met -, oneven met + naar voren
        if(pootnr%2==0 and goingForward): #Pootnr is even en gaat naar voren
                calculate(pootnr, backward[pootnr] - count, heightUp)
                #forwardsMovement(pootnr)
        elif(pootnr%2==0 and not goingForward): #Pootnr is even en gaat naar achter
                calculate(pootnr, forward[pootnr] + count, heightDown)
        elif(pootnr%2==1 and goingForward): #Pootnr is oneven en gaat naar voren
                calculate(pootnr, backward[pootnr] + count, heightUp)
                #forwardsMovement(pootnr)
        elif(pootnr%2==1 and not goingForward): #Pootnr is oneven en gaat naar achter
                calculate(pootnr, forward[pootnr] - count, heightDown)
                

def moveLeg(pootnr, speed):#OBSOLETE
        x.addMovement(pootnr*10+1, alphas[pootnr], speed)
        x.addMovement(pootnr*10+2, betas[pootnr], speed)
        x.addMovement(pootnr*10+3, gammas[pootnr], speed)

def raiseLeg(pootnr):
        calculate(pootnr, backward[pootnr], heightUp)

def lowerLeg(pootnr):
        calculate(pootnr, forward[pootnr], heightDown)

def forwardLeg(pootnr):
        global speed
        placeholder = speed
        speed = 100
        calculate(pootnr, forward[pootnr], heightUp)

sleepTime = 0.005    
def move():
        #lowerLeg(tripodA[0])
        #lowerLeg(tripodA[1])
        #lowerLeg(tripodA[2])
        #moveMultiple()
        #x.sendData()
        #time.sleep(0.5)
        #calcLeg(tripodB[0], 0, False)#calcleg ipv lowerleg
        #calcLeg(tripodB[1], 0, False)
        #calcLeg(tripodB[2], 0, False)
        #moveMultiple()
        #x.sendData()
        #time.sleep(0.2)
        
        raiseLeg(tripodB[0])
        raiseLeg(tripodB[1])
        raiseLeg(tripodB[2])
        moveMultiple()
        x.sendData()
        time.sleep(sleepTime)
        for i in range(0,146, 2): #Tripod A lopen, B reset
                calcLeg(tripodA[0], i, False)
                calcLeg(tripodA[1], i, False)
                calcLeg(tripodA[2], i, False)
                forwardLeg(tripodB[0])
                forwardLeg(tripodB[1])
                forwardLeg(tripodB[2])
                moveMultiple()
                x.sendData()
                time.sleep(sleepTime)
        #lowerLeg(tripodA[0])
        #lowerLeg(tripodA[1])
        #lowerLeg(tripodA[2])
        #moveMultiple()
        #x.sendData()
        #time.sleep(0.5)
        #calcLeg(tripodB[0], 0, False)#calcleg ipv lowerleg
        #calcLeg(tripodB[1], 0, False)
        #calcLeg(tripodB[2], 0, False)
        #moveMultiple()
        #x.sendData()
        #time.sleep(0.2)
        
        raiseLeg(tripodA[0])
        raiseLeg(tripodA[1])
        raiseLeg(tripodA[2])
        moveMultiple()
        x.sendData()
        time.sleep(sleepTime)
        
        for i in range(0,146, 2): #Tripod B lopen, A reset
                calcLeg(tripodB[0], i, False)
                calcLeg(tripodB[1], i, False)
                calcLeg(tripodB[2], i, False)
                forwardLeg(tripodA[0])
                forwardLeg(tripodA[1])
                forwardLeg(tripodA[2])
                moveMultiple()
                x.sendData()
                time.sleep(sleepTime)

def curveMove(stickPosX):
        if(stickPosX <100):
                i = -0.6*stickPosX
                innerLegs = "left"
        elif(stickPosX < 200):
                i = -0.9*stickPosX + 30
                innerLegs = "left"
        elif(stickPosX < 300):
                i = -1.5*stickPosX + 150
                innerLegs = "left"
        elif(stickPosX < 400):
                i = -4*stickPosX + 900
                innerLegs = "left"
        elif(stickPosX < 507):
                i = -100*stickPosX + 39300
                innerLegs = "left"
        elif(stickPosX < 515):
                #vooruitgaande beweging
                move()
                innerLegs = "none"
        elif(stickPosX < 623):
                i = -100 * stickPosX + 63000
                innerLegs = "right"
        elif(stickPosX < 723):
                i = -4 * stickPosX + 3192
                innerLegs = "right"
        elif(stickPosX < 823):
                i = -1.5*stickPosX + 1384.5
                innerLegs = "right"
        elif(stickPosX < 923):
                i = -0.9*stickPosX + 890.7
                innerlegs = "right"
        elif(stickPosX < 1023):
                i = -0.6*stickPosX + 613.8
                innerlegs = "right"

        #i is de lengte tot het denkbeeldige draaipunt
        
        
        
def move2(): #move van initial position naar het andere uiterste, en weer terug.
        #calcLeg(0, 72, False)
        #calcLeg(2, 72, False)
        #calcLeg(3, 72, False)
        #calcLeg(4, 72, False)
        #calcLeg(5, 72, False)
        for i in range(0, 146, 2):#205): #A naar achter, 
                #Tripod A (poten 1,2,5) naar achter
                 #Bereken hoeken servos
                calcLeg(1, i, False)
                calcLeg(2, i, False)
                calcLeg(5, i, False)
                 #Beweeg servos
                #moveLeg(1, speed)
                #moveLeg(2, speed)
                #moveLeg(5, speed)

                 #Tripod B (poten 0,3,4) naar voren
                calcLeg(0, i, True)
                calcLeg(3, i, True)
                calcLeg(4, i, True)     
                 #Beweeg servos
                #moveLeg(0, speed)
                #moveLeg(3, speed)
                #moveLeg(4, speed)
                moveMultiple()

                x.sendData()
                time.sleep(0.01) #Hoe kleiner dit getal, hoe sneller de beweging. (Moet nog een global variable voor komen)
        #time.sleep(0.1)#Een kleine pauze als beide tripods op de grond staan
        for i in range(0, 146, 2):
                #Tripod A (poten 1,2,5) naar voren
                 #Bereken hoeken servos
                calcLeg(1, i, True)
                calcLeg(2, i, True)
                calcLeg(5, i, True)
                 #Beweeg servos
                #moveLeg(1, speed)
                #moveLeg(2, speed)
                #moveLeg(5, speed)

                 #Tripod B (poten 0,3,4) naar achter
                calcLeg(0, i, False)
                calcLeg(3, i, False)
                calcLeg(4, i, False)
                 #Beweeg servos
                #moveLeg(0, speed)
                #moveLeg(3, speed)
                #moveLeg(4, speed)
                moveMultiple()

                x.sendData()
                time.sleep(0.01) #Hoe kleiner dit getal, hoe sneller de beweging. (Moet nog een global variable voor komen)
        #time.sleep(0.1)#Een kleine pauze als beide tripods op de grond staan

#Beweegt alle servos naar de waardes uitgerekend door calculate
def moveMultiple(): 
	servoNrs = [1,2,3,11,12,13,21,22,23,31,32,33,41,42,43,51,52,53]
	positions = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        for i in range(0, len(alphas)):
                j = len(alphas)-i
                positions[i*3] = alphas[i]
                positions[i*3+1] = betas[i]
                positions[i*3+2] = gammas[i]
        x.addMultiple(servoNrs, positions, speed)
        x.sendData()

minTwistHeight = 50
diffTwistHeight = 130
stickAngle = [330, 30, 270, 90, 210, 150]

def calcTwisterLeg(pootnr, height):
        calculate(pootnr, mid[pootnr], height)

def calcTwisterHeight(x):
        height = minTwistHeight + (diffTwistHeight * (((math.cos(x-math.pi))/2)+0.5))
        return height
                 

def twister():
        for i in range(0,360):
                for j in range(0,6):
                        relativeAngle = math.radians(stickAngle[j] - i)
                        height = calcTwisterHeight(relativeAngle)
                        calcTwisterLeg(j, height)
                moveMultiple()
                x.sendData()
                time.sleep(0.001)
def threaten(height):
        calcLeg(0, 72, False)
        calcLeg(1, 72, False)
        calcLeg(4, 72, False)
        calcLeg(5, 72, False)

        global w
        placeholder = w
        w = 200
                
        calculate(2, mid[2], -150 + height)
        calculate(3, mid[3], -150 + height)
        w = placeholder
        moveMultiple()
        x.sendData()
        time.sleep(0.001)

#-------------------------------# MOETEN GETEST WORDEN!
#       TRANSLATE BODY		# NB: Testen of methodes tegelijk kunnen werken, 
#-------------------------------# zo ja dan kunnen ze gebundeld in een transXYZ-methode
# Voor alle drie translate-methodes is het een goede praktijk om bij gebruik altijd
# als laatste weer terug te keren naar de oorspronkelijke waarde. Deze zijn als volgt:
# transX --> w
# transY --> h
# transZ --> alpha (De hele array!)
# Het is dus handig om bij gebruik van deze methodes de oorspronkelijke waardes in een
# lokale variabele op te slaan.
# NB: Deze methodes berekenen alleen - moveMultiple moet apart worden aangeroepen om ook iets te bewegen!

# Deze functie beweegt het body zijwaarts terwijl de poten blijven staan
# Dit wordt gedaan door de 'w'-waarde van de poten individueel te veranderen,
# in plaats van deze voor allen gelijk is (Zoals bij een loop-beweging).
# Bij transValue = 0 blijft de w-waarde gelijk.
def transX(transValue): #Positieve waarden bewegen het body naar links, negatief naar rechts
		global w
		oldw = w
		#Bereken van de linkerpoten de nieuwe positie
		w = oldw - transValue
		calculate(0, alphas[0], legHeight[0])
		calculate(2, alphas[2], legHeight[2])
		calculate(4, alphas[4], legHeight[4])
		#Bereken van de rechterpoten de nieuwe positie
		w = oldw + transValue
		calculate(1, alphas[1], legHeight[1])
		calculate(3, alphas[3], legHeight[3])
		calculate(5, alphas[5], legHeight[5])
		#Reset w naar de oude waarde
		w = oldw

# Deze functie beweegt het body op en neer, terwijl de poten blijven staan.
# Dit wordt gedaan door de h-waarde te veranderen voor alle poten, in plaats van individueel.
# Bij transValue = 0 blijft de h-waarde gelijk.
#ef transY(transValue): #Positief is omhoog, negatief is omlaag
		
		
# Deze functie beweegt het body naar voor en naar achter, terwijl de poten blijven staan.
# Dit wordt gedaan door de alpha-waarde te veranderen voor alle poten, in plaats van individueel.
# Bij transValue = 0 blijft de alpha-waarde gelijk.
def transZ(transValue): #Positief is vooruit, negatief is achteruit
		max = 100 #TODO: Hier een betere waarde voor verzinnen
		if(transValue > max):
				transValue = max
		for i in range(0,6):
				if(i in leftLegs):
					calculate(i, alphas[i] + transValue, legHeight[i])
				else:
					calculate(i, alphas[i] - transValue, legHeight[i])
def transXYZ(x, y, z):
        global w
        oldw = w
        for i in range(0,6):
                if(i in leftLegs):
                       w = oldw - x
                       calculate(i, alphas[i] + z, legHeight[i] + y)
                else:
                       w = oldw + x
                       calculate(i, alphas[i] - z, legHeight[i] + y)
        moveMultiple()
        #Reset waardes
        for i in range(0,6):
                if(i in leftLegs):
                       w = oldw - x
                       calculate(i, alphas[i] - z, legHeight[i] - y)
                else:
                       w = oldw + x
                       calculate(i, alphas[i] + z, legHeight[i] - y) 
        
        #Reset w naar de oude waarde
        w = oldw

#-------------------------------#
#               MAIN		#
#-------------------------------#
        
midPosition()
#initPosition()

#while(True):
        #transXYZ(40, 40, 0)
        #time.sleep(1)
        #transXYZ(0, 0, 0)
        #time.sleep(1)
        #transXYZ(-40, 40, 0)
        #time.sleep(1)
	#move()
        #twister()
        #for i in range(0,75, 1):
        #        threaten(i)
        #for i in range(75,0, -1):
        #        threaten(i)

        


                








