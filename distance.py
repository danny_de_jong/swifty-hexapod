import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)

# The Distance class
class Distance():
   TRIG = 23  # Pins raspberry
   ECHO = 24  # Pins rasberry
   distance = 0.00

   # The distance return class
   def getDistance(self):
      GPIO.setup(self.TRIG,GPIO.OUT)
      GPIO.setup(self.ECHO,GPIO.IN)

      print "Distance Measurement in Progress"

      # Do this to put on###############
      GPIO.output(self.TRIG,False)
      print "Waiting for sensor to settle"
      time.sleep(2)
      ##################################

      # Put the sensor on and send
      GPIO.output(self.TRIG, True)
      time.sleep(0.00001)
      GPIO.output(self.TRIG, False) # Put the sensor off

        #
      while GPIO.input(self.ECHO) == 0 :
         pulse_start = time.time()

      while GPIO.input(self.ECHO) ==1 :
         pulse_end = time.time()

        # Substract the pulse_end from the puls_start
      pulse_duration = pulse_end - pulse_start

        # The formule for the distance sensor
      distance = pulse_duration * 17150

        # Two decimals behind the dot
      distance = round(distance, 2)

        # Debuging
      print "Distance:", distance, "cm"
        # Cleanup the pins
      GPIO.cleanup

      # return the distance
      return distance