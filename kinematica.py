from ax12 import Ax12
from ax12sender import Ax12sender
import time
import math

class Kinematica():
        #forward = [511, 512, 409, 614, 307, 716] #Forward limit per poot (diff 205)
        #backward = [716, 307, 614, 409, 512, 511] #Backward limit per poot (diff 205)
        #forward = [526, 497, 424, 599, 322, 701] #Forward limit per poot (diff 175)
        #backward = [701, 322, 599, 424, 497, 526] #Backward limit per poot (diff 175)

        forward = [540, 483, 438, 585, 336, 687] #Forward limit per poot (diff 146)
        backward = [686, 337, 584, 439, 482, 541] #Backward limit per poot (diff 146)

        mid = [613,409,512,512,409,613]

        alphas = [613,409,512,512,409,613]
        betas = [0,0,0,0,0,0]
        gammas = [0,0,0,0,0,0]

        heightUp = 50
        heightDown = 70
        speed = 600

        def __init__(self, alpha, beta, gamma):
            self.alphas = alpha
            self.betas = beta
            self.gammas = gamma

        def kinematics(self,pootnr, alpha, height):
                w = 120
                a = 50
                b = 82.5
                c = 137
                h0 = 50
                h1 = 30
                h = height

                global alphas
                global betas
                global gammas
                
                #1,2,3
                alpha00 = alpha #servo.readPosition(1)
                alpha001 = float((float(alpha00) - self.mid[pootnr]) / 195)
                s00 = w / math.cos(alpha001) - a
                d00 = math.sqrt((s00*s00) + (h*h))
                epsilon00 = math.atan(h/d00)
                beta = math.acos(((c*c) - (b*b) - (d00*d00)) / (-2 * b * d00))
                gamma = math.acos(((d00*d00)-(b*b)-(c*c)) / (-2 * b * c))
                
                
                # De poten die tegelijk bewegen
                if(pootnr%2 ==0):
                        alphas[pootnr] = alpha
                        betas[pootnr] = int(round((512-195 * epsilon00+195*beta)))
                        gammas[pootnr] = int(round((468+195 * gamma)))
                        
                else:
                        alphas[pootnr] = alpha
                        betas[pootnr] = int(round((512+195 * epsilon00-195*beta)))
                        gammas[pootnr] = int(round((555-195 * gamma)))

        # Getter for the alpha values
        def getAlphas(self):
                return self.alphas;

        # Getter for the beta values
        def getBetas(self):
                return self.betas;

        # Getter for the Gamma values
        def getGammas(self):
                return self.gammas;

