#!/usr/bin/env python2.7
# script by Alex Eames http://RasPi.tv
#http://RasPi.tv/2013/how-to-use-soft-pwm-in-rpi-gpio-pt-2-led-dimming-and-motor-speed-control
# Using PWM with RPi.GPIO pt 2 - requires RPi.GPIO 0.5.2a or higher

import RPi.GPIO as GPIO # always needed with RPi.GPIO
from time import sleep  # pull in the sleep function from time module


class Leds:


    def tripodColor(self, r, g , b, tripod):
        GPIO.setmode(GPIO.BCM)  # choose BCM or BOARD numbering schemes. I use BCM

        GPIO.setup(4, GPIO.OUT)# set GPIO 25 as output for white led
        GPIO.setup(5, GPIO.OUT)# set GPIO 24 as output for red led
        GPIO.setup(6, GPIO.OUT)

        GPIO.setup(13, GPIO.OUT)# set GPIO 25 as output for white led
        GPIO.setup(19, GPIO.OUT)# set GPIO 24 as output for red led
        GPIO.setup(26, GPIO.OUT)

        red  = GPIO.PWM(4, 255)    # create object red for PWM on port 4 at 100 Hertz
        green = GPIO.PWM(5, 100)      # create object green for PWM on port 5 at 100 Hertz
        blue = GPIO.PWM(6, 100) 	#  create object blue for PWM on port 6 at 100 Hertz

        red1  = GPIO.PWM(13, 255)    # create object red for PWM on port 4 at 100 Hertz
        green1 = GPIO.PWM(19, 100)      # create object green for PWM on port 5 at 100 Hertz
        blue1 = GPIO.PWM(26, 100) 	#  create object blue for PWM on port 6 at 100 Hertz

        red.start(0)              # start red led on 0 percent duty cycle (off)
        green.start(0)              # green fully on (100%)
        blue.start(0)		 #  start blue

        red1.start(0)              # start red led on 0 percent duty cycle (off)
        green1.start(0)              # green fully on (100%)
        blue1.start(0)		 #  start blue

        # now the fun starts, we'll vary the duty cycle to
        # dim/brighten the leds, so one is bright while the other is dim

        pause_time = 0.02           # you can change this to slow down/speed up

        try:
            if tripod == 'A':
                red.ChangeDutyCycle(r)
                blue.ChangeDutyCycle(b)
                green.ChangeDutyCycle(g)
            elif tripod == 'B':
                red1.ChangeDutyCycle(r)
                blue1.ChangeDutyCycle(b)
                green1.ChangeDutyCycle(g)

        except KeyboardInterrupt:
            red.stop()            # stop the white PWM output
            green.stop()              # stop the red PWM output
            blue.stop()
            GPIO.cleanup()          # clean up GPIO on CTRL+C exit


    def cycleColors(self):
        GPIO.setmode(GPIO.BCM)  # choose BCM or BOARD numbering schemes. I use BCM

        GPIO.setup(4, GPIO.OUT)# set GPIO 25 as output for white led
        GPIO.setup(5, GPIO.OUT)# set GPIO 24 as output for red led
        GPIO.setup(6, GPIO.OUT)
        GPIO.setup(13, GPIO.OUT)# set GPIO 25 as output for white led
        GPIO.setup(19, GPIO.OUT)# set GPIO 24 as output for red led
        GPIO.setup(26, GPIO.OUT)

        red  = GPIO.PWM(4, 255)    # create object red for PWM on port 4 at 100 Hertz
        green = GPIO.PWM(5, 100)      # create object green for PWM on port 5 at 100 Hertz
        blue = GPIO.PWM(6, 100) 	#  create object blue for PWM on port 6 at 100 Hertz

        red1  = GPIO.PWM(4, 255)    # create object red for PWM on port 4 at 100 Hertz
        green1 = GPIO.PWM(5, 100)      # create object green for PWM on port 5 at 100 Hertz
        blue1 = GPIO.PWM(6, 100) 	#  create object blue for PWM on port 6 at 100 Hertz

        red.start(0)              # start red led on 0 percent duty cycle (off)
        green.start(0)              # green fully on (100%)
        blue.start(0)		 #  start blue

        red1.start(0)              # start red led on 0 percent duty cycle (off)
        green1.start(0)              # green fully on (100%)
        blue1.start(0)		 #  start blue

        # now the fun starts, we'll vary the duty cycle to
        # dim/brighten the leds, so one is bright while the other is dim

        pause_time = 0.02           # you can change this to slow down/speed up

        try:
            while True:
                for i in range(10,101):      # 101 because it stops when it finishes 100
                    red.ChangeDutyCycle(i)
                    sleep(pause_time)
                for i in range(100,0,-1):      # from 100 to zero in steps of -1
                    red.ChangeDutyCycle(i)
                    sleep(pause_time)
                for i in range(10,101):      # 101 because it stops when it finishes 100
                    green.ChangeDutyCycle(i)
                    sleep(pause_time)
                for i in range(100,0,-1):      # from 100 to zero in steps of -1
                    green.ChangeDutyCycle(i)
                    sleep(pause_time)
                for i in range(10,101):      # 101 because it stops when it finishes 100
                    blue.ChangeDutyCycle(i)
                    sleep(pause_time)
                for i in range(100,0,-1):      # from 100 to zero in steps of -1
                    blue.ChangeDutyCycle(i)
                    sleep(pause_time)

        except KeyboardInterrupt:
            red.stop()            # stop the white PWM output
            green.stop()              # stop the red PWM output
            blue.stop()
            GPIO.cleanup()          # clean up GPIO on CTRL+C exit

    def fade(self):
        GPIO.setmode(GPIO.BCM)  # choose BCM or BOARD numbering schemes. I use BCM

        GPIO.setup(4, GPIO.OUT)# set GPIO 25 as output for white led
        GPIO.setup(5, GPIO.OUT)# set GPIO 24 as output for red led
        GPIO.setup(6, GPIO.OUT)
        GPIO.setup(13, GPIO.OUT)# set GPIO 25 as output for white led
        GPIO.setup(19, GPIO.OUT)# set GPIO 24 as output for red led
        GPIO.setup(26, GPIO.OUT)

        red  = GPIO.PWM(4, 255)    # create object red for PWM on port 4 at 100 Hertz
        green = GPIO.PWM(5, 100)      # create object green for PWM on port 5 at 100 Hertz
        blue = GPIO.PWM(6, 100) 	#  create object blue for PWM on port 6 at 100 Hertz
        red1  = GPIO.PWM(13, 255)    # create object red for PWM on port 4 at 100 Hertz
        green1 = GPIO.PWM(19, 100)      # create object green for PWM on port 5 at 100 Hertz
        blue1 = GPIO.PWM(26, 100) 	#  create object blue for PWM on port 6 at 100 Hertz

        red.start(0)              # start red led on 0 percent duty cycle (off)
        green.start(100)              # green fully on (100%)
        blue.start(0)		 #  start blue

        red1.start(0)              # start red led on 0 percent duty cycle (off)
        green1.start(100)              # green fully on (100%)
        blue1.start(0)		 #  start blue

        # now the fun starts, we'll vary the duty cycle to
        # dim/brighten the leds, so one is bright while the other is dim

        pause_time = 0.02           # you can change this to slow down/speed up

        try:
            while True:
                for i in range(10,101):      # 101 because it stops when it finishes 100
                    red.ChangeDutyCycle(i)
                    green.ChangeDutyCycle(100 - i)
                    blue.ChangeDutyCycle(100 - i)
                    red1.ChangeDutyCycle(i)
                    green1.ChangeDutyCycle(100 - i)
                    blue1.ChangeDutyCycle(100 - i)
                    sleep(pause_time)
                for i in range(100,-1,-1):      # from 100 to zero in steps of -1
                    red.ChangeDutyCycle(i)
                    green.ChangeDutyCycle(100 - i)
                    blue.ChangeDutyCycle(100 - i)
                    red1.ChangeDutyCycle(i)
                    green1.ChangeDutyCycle(100 - i)
                    blue1.ChangeDutyCycle(100 - i)
                    sleep(pause_time)

        except KeyboardInterrupt:
            red.stop()            # stop the white PWM output
            green.stop()              # stop the red PWM output
            blue.stop()
            red1.stop()            # stop the white PWM output
            green1.stop()              # stop the red PWM output
            blue1.stop()
            GPIO.cleanup()          # clean up GPIO on CTRL+C exit
