__author__ = 'account'

import RPi.GPIO as GPIO
from time import sleep

class Needle:

    needle = None

    def __init__(self):
        GPIO.setmode(GPIO.BCM)  # choose BCM or BOARD numbering schemes. I use BCM
        GPIO.setup(25, GPIO.OUT)
        self.needle  = GPIO.PWM(25, 255)

    def startNeedle(self,heatInPercent):
        while(True):
            self.needle.start(heatInPercent)

    def stopNeedle(self):
        self.needle.stop()


