__author__ = 'account'

# from ax12 import Ax12
# from ax12sender import Ax12sender
import os

import time
import math
# from kinematica import Kinematica
import os
from swifty.move import Move
from leds import Leds
from ax12 import Ax12
from distance import Distance
from needle import Needle

import subprocess

#x = Ax12sender()

#servo = Ax12()

# This is the main spider class
class Spider:
    # Spider states
    # These are bit values for putting the spider in state
    # 0 -> Keyboard    int 128
    # 0 -> free        int 64
    # 0 -> dance       int 32
    # 0 -> spider gap  int 16
    # 0 -> widow       int 8
    # 0 -> pole        int 4
    # 0 -> gravel      int 2
    # 0 -> needle      int 1
    spiderState = "10000000"

    move = Move()
    leds = Leds()
    ax12 = Ax12()
    distance = Distance()
    sensorData = ""
    needle = Needle()

    def __init__(self):
       #move = Move()
       leds = Leds()
       ax12 = Ax12()
       distance = Distance()
       needle = Needle()


    # Running a process
    def runProcess(self,exe):
        p = subprocess.Popen(exe, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        while(True):
          retcode = p.poll() #returns None while subprocess is running
          line = p.stdout.readline() #
          yield line
          if(retcode is not None):
            break

    def startProcess(self, process,*args):
        for line in subprocess.call(process.split()):
                # Check if there is a line
                if line:
                    line.strip() # Strip de line
                    moveArguments = line.split(" ") # Put all the arguments in an array

                    # Give the spider.Move() the arguments
                    # spider.move(moveArguments[0],moveArguments[1])
                    if(len(moveArguments) is not 1):
                        print moveArguments[0] + ":" + moveArguments[1]

    # Read the servo's and the sensor information
    # And write this information to a file named sensorData.txt
    # This file is then read by the connection.py service script wich
    # Waits for incomming connection from client that show data
    def readAndWriteSensorsToFile(self):

        servos = [1,2,3,11,12,13,21,22,23,31,32,33,41,42,43,51,52,53]

        # Read all servo data
        for servo in servos:
            self.sensorData += str(self.ax12.readLoad(servo)) + ":"
            time.sleep(0.02)
            self.sensorData += str(self.ax12.readPosition(servo)) + ":"
            time.sleep(0.02)
            self.sensorData += str(self.ax12.readTemperature(servo)) + ":"
            time.sleep(0.02)

        # Read distance
        #self.sensorData += str(self.distance.getDistance())

        # Open file and write the data to file
        file = open("sensorData.txt","r+")
        file.write(self.sensorData)

        # Return the data for testing purpose
        return self.sensorData
