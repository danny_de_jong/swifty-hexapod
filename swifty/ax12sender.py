from time import sleep
from serial import Serial
import RPi.GPIO as GPIO

class Ax12sender:
    # important AX-12 constants
    # /////////////////////////////////////////////////////////// EEPROM AREA
    AX_MODEL_NUMBER_L = 0
    AX_MODEL_NUMBER_H = 1
    AX_VERSION = 2
    AX_ID = 3
    AX_BAUD_RATE = 4
    AX_RETURN_DELAY_TIME = 5
    AX_CW_ANGLE_LIMIT_L = 6
    AX_CW_ANGLE_LIMIT_H = 7
    AX_CCW_ANGLE_LIMIT_L = 8
    AX_CCW_ANGLE_LIMIT_H = 9
    AX_SYSTEM_DATA2 = 10
    AX_LIMIT_TEMPERATURE = 11
    AX_DOWN_LIMIT_VOLTAGE = 12
    AX_UP_LIMIT_VOLTAGE = 13
    AX_MAX_TORQUE_L = 14
    AX_MAX_TORQUE_H = 15
    AX_RETURN_LEVEL = 16
    AX_ALARM_LED = 17
    AX_ALARM_SHUTDOWN = 18
    AX_OPERATING_MODE = 19
    AX_DOWN_CALIBRATION_L = 20
    AX_DOWN_CALIBRATION_H = 21
    AX_UP_CALIBRATION_L = 22
    AX_UP_CALIBRATION_H = 23

    # ////////////////////////////////////////////////////////////// RAM AREA
    AX_TORQUE_STATUS = 24
    AX_LED_STATUS = 25
    AX_CW_COMPLIANCE_MARGIN = 26
    AX_CCW_COMPLIANCE_MARGIN = 27
    AX_CW_COMPLIANCE_SLOPE = 28
    AX_CCW_COMPLIANCE_SLOPE = 29
    AX_GOAL_POSITION_L = 30
    AX_GOAL_POSITION_H = 31
    AX_GOAL_SPEED_L = 32
    AX_GOAL_SPEED_H = 33
    AX_TORQUE_LIMIT_L = 34
    AX_TORQUE_LIMIT_H = 35
    AX_PRESENT_POSITION_L = 36
    AX_PRESENT_POSITION_H = 37
    AX_PRESENT_SPEED_L = 38
    AX_PRESENT_SPEED_H = 39
    AX_PRESENT_LOAD_L = 40
    AX_PRESENT_LOAD_H = 41
    AX_PRESENT_VOLTAGE = 42
    AX_PRESENT_TEMPERATURE = 43
    AX_REGISTERED_INSTRUCTION = 44
    AX_PAUSE_TIME = 45
    AX_MOVING = 46
    AX_LOCK = 47
    AX_PUNCH_L = 48
    AX_PUNCH_H = 49

    # /////////////////////////////////////////////////////////////// Status Return Levels
    AX_RETURN_NONE = 0
    AX_RETURN_READ = 1
    AX_RETURN_ALL = 2

    # /////////////////////////////////////////////////////////////// Instruction Set
    AX_PING = 1
    AX_READ_DATA = 2
    AX_WRITE_DATA = 3
    AX_REG_WRITE = 4
    AX_ACTION = 5
    AX_RESET = 6
    AX_SYNC_WRITE = 131

    # /////////////////////////////////////////////////////////////// Lengths
    AX_RESET_LENGTH = 2
    AX_ACTION_LENGTH = 2
    AX_ID_LENGTH = 4
    AX_LR_LENGTH = 4
    AX_SRL_LENGTH = 4
    AX_RDT_LENGTH = 4
    AX_LEDALARM_LENGTH = 4
    AX_SHUTDOWNALARM_LENGTH = 4
    AX_TL_LENGTH = 4
    AX_VL_LENGTH = 6
    AX_AL_LENGTH = 7
    AX_CM_LENGTH = 6
    AX_CS_LENGTH = 5
    AX_COMPLIANCE_LENGTH = 7
    AX_CCW_CW_LENGTH = 8
    AX_BD_LENGTH = 4
    AX_TEM_LENGTH = 4
    AX_MOVING_LENGTH = 4
    AX_RWS_LENGTH = 4
    AX_VOLT_LENGTH = 4
    AX_LOAD_LENGTH = 4
    AX_LED_LENGTH = 4
    AX_TORQUE_LENGTH = 4
    AX_POS_LENGTH = 4
    AX_GOAL_LENGTH = 5
    AX_MT_LENGTH = 5
    AX_PUNCH_LENGTH = 5
    AX_SPEED_LENGTH = 5
    AX_GOAL_SP_LENGTH = 7

    # /////////////////////////////////////////////////////////////// Specials
    AX_BYTE_READ = 1
    AX_INT_READ = 2
    AX_ACTION_CHECKSUM = 250
    AX_BROADCAST_ID = 254
    AX_START = 255
    AX_CCW_AL_L = 255
    AX_CCW_AL_H = 3
    AX_LOCK_VALUE = 1
    LEFT = 0
    RIGTH = 1
    RX_TIME_OUT = 10
    TX_DELAY_TIME = 0.00002
    DELAY_AFTER_SEND = 0.002

    # RPi constants
    RPI_DIRECTION_PIN = 18
    RPI_DIRECTION_TX = GPIO.HIGH
    RPI_DIRECTION_RX = GPIO.LOW
    RPI_DIRECTION_SWITCH_DELAY = 0.0001

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(18, GPIO.OUT)

    # static variables
    port = None
    gpioSet = False
    
    def __init__(self):
        if(Ax12sender.port == None):
            Ax12sender.port = Serial("/dev/ttyAMA0", baudrate=1000000, timeout=0.001)
        if(not Ax12sender.gpioSet):
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(Ax12sender.RPI_DIRECTION_PIN, GPIO.OUT)
            Ax12sender.gpioSet = True
        self.direction(Ax12sender.RPI_DIRECTION_RX)
        self.outData = ""
        self.dataLength = 0
        self.packetsize = 4 + 1
        self.checksum = 0
    
    def direction(self,d):
        GPIO.output(Ax12sender.RPI_DIRECTION_PIN, d)
        sleep(Ax12sender.RPI_DIRECTION_SWITCH_DELAY)
    
    def addMovement(self, servoNr, position, speed):
        p = [position&0xff, position>>8]
        s = [speed&0xff, speed>>8]
        self.outData += chr(servoNr)
        self.checksum += servoNr
        self.outData += chr(p[0])
        self.checksum += p[0]
        self.outData += chr(p[1])
        self.checksum += p[1]
        self.outData += chr(s[0])
        self.checksum += s[0]
        self.outData += chr(s[1])
        self.checksum += s[1]
        self.dataLength += 1
	
    def addMultiple(self, servoNrs, positions, speed):
	
	for i in range(0,len(servoNrs)):
		p = [positions[i]&0xff, positions[i]>>8]
        	s = [speed&0xff, speed>>8]
		self.outData += chr(servoNrs[i])
	        self.checksum += servoNrs[i]
        	self.outData += chr(p[0])
        	self.checksum += p[0]
        	self.outData += chr(p[1])
        	self.checksum += p[1]
        	self.outData += chr(s[0])
        	self.checksum += s[0]
        	self.outData += chr(s[1])
        	self.checksum += s[1]
        self.dataLength = len(servoNrs)
    
#    def addMultiple(self,Data,Checksum, dataLength):
#        self.outData += Data
#        self.checksum +=Checksum
#        self.dataLength += dataLength
        
    def sendData(self):
        self.direction(Ax12sender.RPI_DIRECTION_TX)
        Ax12sender.port.flushInput()
        tosend = chr(Ax12sender.AX_START)
        tosend += chr(Ax12sender.AX_START)
        tosend += chr(254) #Sync-write commando id
        self.checksum += 254
        tosend += chr(self.packetsize * self.dataLength + 4) #Hoeveelheid parameters (L+1) * N + 4
        self.checksum += self.packetsize * self.dataLength + 4
        tosend += chr(131) #Instruction
        self.checksum += 131
        tosend += chr(30) #Starting Adress
        self.checksum += 30
        tosend += chr(4) #Length of data (L)
        self.checksum += 4
        tosend += self.outData
        Checksum = 255 - (self.checksum % 256)
        tosend += chr(Checksum)
        Ax12sender.port.write(tosend)
        sleep(Ax12sender.DELAY_AFTER_SEND)
        self.outData = ""
        self.dataLength = 0
        self.checksum = 0
        return 0 #self.readData(id)
        
    def close(self):
        GPIO.cleanup()
        Ax12sender.port.close()
