import time
import math
from leds import Leds
from ax12sender import Ax12sender
from stance import Stance

class Move:
        def __init__(s):
            s.stance = Stance()
            s.leds = Leds()
            s.forward = [540, 483, 438, 585, 336, 687] #Forward limit per poot (difference 146)
            s.backward = [686, 337, 584, 439, 482, 541] #Backward limit per poot (difference 146)
            s.mid = [613,409,512,512,409,613]

            s.lower = [s.forward[0], s.backward[1], s.forward[2], s.backward[3], s.forward[4], s.backward[5]]
            s.upper = [s.backward[0], s.forward[1], s.backward[2], s.forward[3], s.backward[4], s.forward[5]]
    
            #s.mid = [613,409,512,512,409,613]
            #s.maxDifference = 146	#Grootte van de loopbeweging, gemeten in servo-orientatie-waardes (1024 = 300 graden)
            s.maxDifference = 132
            s.stepSize = 6		#Hoe kleiner, hoe rechter. Hoe groter, hoe dichter bij de opgegeven 's.speed'
            #als maxDifference%s.stepSize niet 0 is, schiet het pootje in de loopbeweging iets uit!
    
            s.tripod = {}
            s.tripod['A'] = [0, 3, 4] #Pootnummers van de tripods
            s.tripod['B'] = [1, 2, 5] #Als dit array-gebruik problemen geeft, dan alle occurences weer terug-veranderen naar 'tripodA' en 'tripodB'
            # s.leftLegs = [0,2,4]
            # s.rightLegs = [1,3,5]
            s.leftLegs = [1,3,5]
            s.rightLegs = [0,2,4]
    
            s.alphas = [613,409,512,512,409,613]
            s.betas = [0,0,0,0,0,0]
            s.gammas = [0,0,0,0,0,0]
    
            s.heightUp = 70	#Standaard-hoogte voor de loopbeweging (Poot omhoog)
            s.heightDown = 100	#Standaard-hoogte voor de loopbeweging (Poot omlaag)
            s.stepHeightUp = 135
            s.stepHeightDown = 165
            s.w = s.stance.w  #standaard w
    
            s.legHeight = [s.heightDown,s.heightDown,s.heightDown,s.heightDown,s.heightDown,s.heightDown] #hoogte per poot. Standaardwaarde (Voor de eerste 'calculate') is 's.heightDown'
            s.legWidth = [s.w,s.w,s.w,s.w,s.w,s.w] #afstand tot servo alpha per poot. Standaardwaarde is w

            s.minSpeed = 300 #minimum speed
            s.maxSpeed = 700 #maximum speed - minimum
            
            s.lowTripod = 'A'
            s.highTripod = 'B'
            s.innerLegs = "none"
            s.stepDifference = s.maxDifference
            
            s.diffOverload = s.maxDifference * 2.5 #Arbitraire vermenigvuldigings-factor
            s.difference = [s.maxDifference, s.maxDifference, s.maxDifference, s.maxDifference, s.maxDifference, s.maxDifference]
            #s.currDifference = [int(s.maxDifference*0.1), int(s.maxDifference*0.9), int(s.maxDifference*0.8), int(s.maxDifference * 0.2), int(s.maxDifference *0.3), int(s.maxDifference*0.7)]
            #s.currDifference = [int(s.maxDifference/3*1), int(s.maxDifference/3*3), int(s.maxDifference/3*3), int(s.maxDifference/3*0), int(s.maxDifference/3*0), int(s.maxDifference/3*2)]
            # s.currDifference = [0, s.maxDifference, s.maxDifference, 0, 0, s.maxDifference] #Arbitraire startpositie (tripod)
            s.currDifference = [int(s.maxDifference/3*3), int(s.maxDifference/3*1), int(s.maxDifference/3*0), int(s.maxDifference/3*3), int(s.maxDifference/3*2), int(s.maxDifference/3*0)]

            
            s.recordalphas = {}
            s.recordbetas = {}
            s.recordgammas = {}
            s.recordW = s.w

            s.controlScheme = 0
            s.stickMoveCalled = False
            s.backStep = False
            s.turnStepCount = 0

            s.minTwistHeight = 50
            s.diffTwistHeight = 130
            s.stickAngle = [330, 30, 270, 90, 210, 150]


            s.sleepTime = 0.01 #Sleep tijd tussen berekeningen moveRipple
            s.bufferTime = 0.11
            s.bufferCycles = s.bufferTime / s.sleepTime #Eerste waarde is beoogde beweegtijd in seconden
            # s.legRaised = [0, s.bufferCycles, s.bufferCycles, 0, 0, s.bufferCycles] #tripod stand (moet getest worden)
            #s.legRaised = [0, s.bufferCycles, 0, 0, s.bufferCycles*2, 0]
            s.legRaised = [s.bufferCycles, 0, 0, 0, 0, s.bufferCycles*2]
            s.deactivatedLegs = {}
            s.liftedLegs = 0
            s.bufferDifference = [0,0,0,0,0,0]
            for i in range(0,6):
                s.bufferDifference[i] = s.diffOverload
    
            #WIJZIG CURRDIFFERENCE VOOR EEN ANDERE GAIT (moet getest worden)

            s.x = Ax12sender()

            ##################
            #    MAIN        #
            ##################
            print("Voor while-loop")
            s.stance.oval()
            s.moveMultiple()

            #while(True):
             #   for i in range(0,3):
             #   s.stickTesting
             #       s.testCrab()
             #   for i in range(0,3):
             #       s.sharpTurn("right")
                #s.sharpTurn("left")
                #s.stickMove(212, 512)
                #s.crabWalk("left")
                #s.move()
                #s.testing()
                #s.moveMultiple()
                #s.moveRipple()
                #time.sleep(2)

        def calculate(s, pootnr, alpha, height):
            s.stance.calculate(pootnr, alpha, height)

        def moveMultiple(s):
            s.stance.moveMultiple()

        def calcLeg(s, pootnr, count):
                if(pootnr in s.leftLegs):
                        s.calculate(pootnr, s.mid[pootnr] + s.difference[pootnr]/2 - count, s.heightDown)
                elif(pootnr in s.rightLegs):
                        s.calculate(pootnr, s.mid[pootnr] - s.difference[pootnr]/2 + count, s.heightDown)

        def raiseLeg(s, pootnr):
                if(pootnr in s.leftLegs):
                        s.calculate(pootnr, s.mid[pootnr] - s.currDifference[pootnr]/2, s.heightUp)
                elif(pootnr in s.rightLegs):
                        s.calculate(pootnr, s.mid[pootnr] + s.currDifference[pootnr]/2, s.heightUp)

        def lowerLeg(s, pootnr):
                if(pootnr in s.leftLegs):
                        s.calculate(pootnr, s.mid[pootnr] + s.difference[pootnr]/2, s.heightDown)
                elif(pootnr in s.rightLegs):
                        s.calculate(pootnr, s.mid[pootnr] - s.difference[pootnr]/2, s.heightDown)

        def forwardLegDir(s, pootnr, direction):#, diff):

                if(pootnr in s.leftLegs or (pootnr in s.rightLegs and direction == "backward")):
                        s.calculate(pootnr, s.mid[pootnr] + s.difference[pootnr]/2, s.heightUp)
                elif(pootnr in s.rightLegs or (pootnr in s.leftLegs and direction == "backward")):
                        s.calculate(pootnr, s.mid[pootnr] - s.difference[pootnr]/2, s.heightUp)
                #calculate(pootnr, mid[pootnr] + diff/2, s.heightUp)

        def forwardLeg(s, pootnr ):#, diff):
                placeholder = s.stance.speed
                s.stance.setSpeed(900)
                if(pootnr in s.leftLegs):
                        s.calculate(pootnr, s.mid[pootnr] + s.difference[pootnr]/2, s.heightUp)
                elif(pootnr in s.rightLegs):
                        s.calculate(pootnr, s.mid[pootnr] - s.difference[pootnr]/2, s.heightUp)
                #calculate(pootnr, mid[pootnr] + diff/2, s.heightUp)
                s.stance.setSpeed(placeholder)

        def setMoveSpeed(s, diff):
            x = math.radians((diff/float(s.maxDifference))*360)
            speed = s.minSpeed + int(s.maxSpeed * ((math.cos(x-math.pi)/2)+0.5))
            print("speed: " + str(speed))
            s.stance.setSpeed(speed)
        
        def move(s, direction): #direction is "forward" of "backward"
            sleepTime = 0.003 #seconden
            stepDiffOffset = s.stepDifference / 2
            setStartCount = False

            placeholderDown = s.heightDown
            placeholderUp = s.heightUp

            #s.leds.tripodColor(100,0,0, s.highTripod)
            #s.leds.tripodColor(0,0,100, s.lowTripod)

            if direction == "forward":
                s.leftLegs = [1,3,5]
                s.rightLegs = [0,2,4]
            elif direction == "backward":
                s.leftLegs = [0,2,4]
                s.rightLegs = [1,3,5]

            s.raiseLeg(s.tripod[s.highTripod][0]) # Beweeg de niet-lopende tripod omhoog
            s.raiseLeg(s.tripod[s.highTripod][1])
            s.raiseLeg(s.tripod[s.highTripod][2])
            s.moveMultiple()
            time.sleep(sleepTime*10)

            s.forwardLeg(s.tripod[s.highTripod][0]) # Beweeg de niet-lopende tripod vooruit
            s.forwardLeg(s.tripod[s.highTripod][1])
            s.forwardLeg(s.tripod[s.highTripod][2])
            for i in range(0, s.maxDifference, s.stepSize): #Beweeg de lopende tripod volgens kinematica naar achteren
                    s.setMoveSpeed(i)
                    if(s.innerLegs == "left"):
                            for j in s.tripod[s.lowTripod]:
                                    if(j in s.leftLegs):
                                            if(i> s.maxDifference/2-stepDiffOffset and i<s.maxDifference/2+stepDiffOffset):
                                                if j in s.deactivatedLegs:
                                                    s.heightUp = s.stepHeightUp
                                                    s.heightDown = s.stepHeightDown
                                                    s.calcLeg(j, i - int(s.maxDifference / 2 - stepDiffOffset))
                                                    s.heightDown = placeholderDown
                                                    s.heightUp = placeholderUp
                                                else:
                                                    s.calcLeg(j, i - int(s.maxDifference / 2 - stepDiffOffset))

                                    else:
                                        if j in s.deactivatedLegs:
                                            s.heightUp = s.stepHeightUp
                                            s.heightDown = s.stepHeightDown
                                            s.calcLeg(j, i)
                                            s.heightDown = placeholderDown
                                            s.heightUp = placeholderUp
                                        else:
                                            s.calcLeg(j, i)
                    elif(s.innerLegs == "right"):
                            for j in s.tripod[s.lowTripod]:
                                    if(j in s.leftLegs):
                                            if(i>s.maxDifference/2-stepDiffOffset and i<s.maxDifference/2+stepDiffOffset):
                                                if j in s.deactivatedLegs:
                                                    s.heightUp = s.stepHeightUp
                                                    s.heightDown = s.stepHeightDown
                                                    s.calcLeg(j, i - int(s.maxDifference / 2 - stepDiffOffset))
                                                    s.heightDown = placeholderDown
                                                    s.heightUp = placeholderUp
                                                else:
                                                    s.calcLeg(j, i-int(s.maxDifference / 2 - stepDiffOffset))
                                    else:
                                        if j in s.deactivatedLegs:
                                            s.heightUp = s.stepHeightUp
                                            s.heightDown = s.stepHeightDown
                                            s.calcLeg(j, i)
                                            s.heightDown = placeholderDown
                                            s.heightUp = placeholderUp
                                        else:
                                            s.calcLeg(j, i)
                    else:
                            for j in s.tripod[s.lowTripod]:
                                if j in s.deactivatedLegs:
                                    s.heightUp = s.stepHeightUp
                                    s.heightDown = s.stepHeightDown
                                    s.calcLeg(j, i)
                                    s.heightDown = placeholderDown
                                    s.heightUp = placeholderUp
                                else:
                                    s.calcLeg(j, i)


                    s.moveMultiple()
                    time.sleep(sleepTime)
            s.lowerLeg(s.tripod[s.highTripod][0])# Laat de niet-lopende tripod zakken
            s.lowerLeg(s.tripod[s.highTripod][1])# Nu staan alle poten aan de grond
            s.lowerLeg(s.tripod[s.highTripod][2])
            s.moveMultiple()
            time.sleep(sleepTime*10)
            # Verwissel de tripod-designaties voor de volgende stap
            if(s.lowTripod == 'A'):
                s.lowTripod = 'B'
                s.highTripod = 'A'
            elif(s.lowTripod == 'B'):
                s.lowTripod = 'A'
                s.highTripod = 'B'

        def setWalkHeight(s, diff):
            s.heightDown += diff/2
            s.heightUp -= diff/2

        def setBodyHeight(s, diff):
            s.heightDown += diff
            s.heightUp += diff

            for i in range(0,6):
                s.calculate(i, s.stance.alphas[i], s.stance.legHeight[i] + diff)
            s.moveMultiple()

        def toggleCrabwalk(s): #Aangeroepen door een van de knopjes op de controller
            if s.controlScheme:
                s.controlScheme = False
            else:
                s.controlScheme = True

        def poleDance(s):
            s.stance.middle()
            s.move("forward")
            s.sharpTurn("right")
            s.sharpTurn("right")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")
            s.move("forward")
            s.move("forward")
            s.move("forward")
            s.sharpTurn("left")


        def stickMove(s, input):
            #Controlscheme-loze buttons
            if input == 'z':
                #Standaard walk settings
                print("# Standard walk activated.")
                s.maxDifference = 132
                s.stepSize = 6
                s.sleepTime = 0.01
                s.heightDown = 100
                s.heightUp = 70
            if input == 'x':
                #Dance walk settings
                print("# Short step march activated.")
                s.maxDifference = 50 #Testen
                s.stepSize = 6
                s.sleepTime = 0.01
                s.heightDown = 130
                s.heightUp = 70
            if input == 'c':
                #Moonwalk settings
                print("# Moonwalk activated.")
                s.maxDifference = 90 #Testen
                s.stepSize = 6
                s.sleepTime = 0.01
                s.heightDown = 80
                s.heightUp = 70
            if input == 'v':
                # Hoge walk settings
                print("# Tip-toe activated.")
                s.maxDifference = 120 # Testen
                s.stance.w = 70
                s.stepSize = 6
                s.sleepTime = 0.01
                s.heightDown = 150
                s.heightUp = 120
            if input == 'b':
                # Buik-schuiver settings
                print("# Lowrider activated.")
                s.maxDifference = 144
                s.stance.w = 60 #Testen op math domain error
                s.stepSize = 3
                s.sleepTime = 0.01
                s.heightDown = 40
                s.heightUp = 30
            if input == 'n':
                # Marcheer in plaats
                print("# Standing march activated.")
                s.maxDifference = 1
                s.stepSize = 1
                s.sleepTime = 0.2
                s.heightDown = 100
                s.heightUp = 70
            if input == '.':
                s.stance.liftOneLeg()


            elif s.controlScheme == 0 and len(input) == 1: #STANDARD SCHEME
                #control scheme knop
                if(input == 'S'):
                    s.controlScheme = 1
                    print("# Switching to TWISTER scheme")
                    return
                #tripod move WASD controls
                elif(input == 'w'):
                    s.move("forward")
                elif(input == 's'):
                    s.move("backward")
                elif(input == 'a'):
                    s.sharpTurn("left")
                elif(input == 'd'):
                    s.sharpTurn("right")
                elif(input == 'q'):
                    s.testCrab()
                elif(input == 'e'):
                    s.testCrab()
                elif(input == '9'):
                    s.stance.prick()
                elif(input == '0'):
                    s.stance.prick2()

                #preset stances
                elif(input == 'p'):
                    s.stance.middle()
                elif(input == 'o'):
                    s.stance.oval()
                elif(input == 'i'):
                    s.stance.hands()

                #translate numpad controls
                elif(input == '8'):
                    s.stance.transXYZ(0,0,9)
                elif(input == '2'):
                    s.stance.transXYZ(0,0,-9)

                elif(input == '4'):
                    s.stance.transXYZ(9,0,0)
                elif(input == '6'):
                    s.stance.transXYZ(-9,0,0)

                elif(input =='+'):
                    s.setWalkHeight(6)
                elif(input =='-'):
                    s.setWalkHeight(-0)
                elif(input =='*'):
                    s.setBodyHeight(6)
                elif(input =='/'):
                    s.setBodyHeight(-6)

                #Overig
                elif(input == '['):
                    s.twister()
                elif(input == ']'):
                    s.stance.twisterLift()
            elif(s.controlScheme == 1 and len(input) == 1): #TWISTER SCHEME
                #control scheme knop
                if(input == 'S'):
                    s.controlScheme = 2
                    print("# Switching to SPIDERGAP scheme.")
                    return
                elif(input == '7'):
                    s.twister2(330)
                elif(input == '8'):
                    s.twister2(360)
                elif(input == '9'):
                    s.twister2(30)
                elif(input == '6'):
                    s.twister2(90)
                elif(input == '3'):
                    s.twister2(150)
                elif(input == '2'):
                    s.twister2(180)
                elif(input == '1'):
                    s.twister2(210)
                elif(input == '4'):
                    s.twister2(270)
                elif(input == '5'):
                    s.stance.middle()
                elif(input == '['):
                    s.twister()
                elif(input == ']'):
                    s.stance.twisterLift()
                #tripod move WASD controls
                elif(input == 'w'):
                    s.move("forward")
                elif(input == 's'):
                    s.move("backward")
                elif(input == 'a'):
                    s.sharpTurn("left")
                elif(input == 'd'):
                    s.sharpTurn("right")
                elif(input == 'q'):
                    s.testCrab()
                elif(input == 'e'):
                    s.testCrab()
            elif(s.controlScheme == 2 and len(input) == 1): #SPIDERGAP SCHEME
                #control scheme knop
                if(input == 'S'):
                    s.controlScheme = 0
                    print("# Switching to STANDARD theme.")
                    return
                elif(input == '+'):
                    #Raise leg
                    if s.liftedLegs <= 6:
                        s.liftedLegs += 1
                    for i in range(0, s.liftedLegs):
                        s.deactivatedLegs[i] = i
                    if s.liftedLegs == 7:
                        s.liftedLegs = 0
                elif(input == '-'):
                    #Lower leg
                    if s.liftedLegs > 0:
                        s.liftedLegs -= 1
                    for i in range(0, s.liftedLegs):
                        s.deactivatedLegs[i] = i
                elif(input == 'w'):
                    s.move("forward")
                elif(input == 's'):
                    s.move("backward")
                elif(input == 'a'):
                    s.sharpTurn("left")
                elif(input == 'd'):
                    s.sharpTurn("right")
                elif(input == 'e'):
                    s.testCrab()




            else: #Commandos
                if input == "help":
                    print("#-------#  swiftDOS HELP  #-------#")
                    print("# ")
                    print("# swiftDOS helps you debug SWIFTY (c) and perfect its functions.")
                    print("# The following commands can be used: ")
                    print("#     help     - Return to this screen")
                    print("#     controls - Show current control scheme")
                    print("#     settings - Change internal variables without powering down")
                    print("")
                elif input == "controls":
                    print("#-------#  Control scheme: 0 [STANDARD]  #-------#")
                    print("# ")
                    print("# MOVEMENT")
                    print("#    w - forward")
                    print("#    s - backward")
                    print("#    a - sharp turn left")
                    print("#    d - sharp turn right")
                    print("#    q - strafe left")
                    print("#    e - strafe right")
                    print("# ")
                    print("# STANCES")
                    print("#    o - oval stance")
                    print("#    p - middle stance")
                    print("#    i - hands stance")
                    print("# ")
                    print("# BODY TRANSLATION")
                    print("#    4, 6 - translate X")
                    print("#    8, 2 - translate Z")
                    print("# ")
                    print("# HEIGHT MODIFICATION")
                    print("#    -, + - Change walking height")
                    print("#    /, * - Change body height")
                    print("# ")
                    print("# MISCELLANEOUS")
                    print("#    [ - twister move")
                    print("#    ] - twisterLift move")
                    print("")
                elif input == "help settings":
                    print("#-------#  swiftDOS SETTINGS INFO  #-------#")
                    print("# ")
                    print("# The relevant variables are: ")
                    print("#     a. maxDifference  - The alpha-servo movement limit")
                    print("#     b. stepSize       - Determines the amount of calculations per step")
                    print("#     c. sleepTime      - Smaller buffertime to allow for physical movement during drag")
                    print("#     d. bufferTime     - Larger buffertime to allow for physical movement during lift")
                    print("#     e. difference     - Individual alpha-servo movement limits for during rippleMove")
                    print("#     f. legRaised      - Logs legstatus during rippleMove")
                    print("#     g. currDifference - Logs alpha-servo status during rippleMove")
                    print("#     h. heightUp       - H-value for raised legs")
                    print("#     i. heightDown     - H-value for lowered legs")
                    print("#     j. w              - Horizontal distance between the feet and the alpha-servo")
                    print("#     k. minSpeed       - Minimal servospeed")
                    print("#     l. maxSpeed       - Maximum servospeed, minus minSpeed")
                    print("#     m. liftHeight     - Hoogte die de pootjes opgetilt worden tijdens twisterLift")
                    print("# ")
                    print("# To view the current values and make changes, use the command 'settings'")
                    print("# To modify only the values relevant to moveRipple, use the command 'settings ripple'")
                    print("")
                elif input == "settings":
                    print("#-------#  swiftDOS SETTINGS #-------#")
                    print("# ")
                    print("#     a. maxDifference  - " + str(s.maxDifference))
                    print("#     b. stepSize       - " + str(s.stepSize))
                    print("#     c. sleepTime      - " + str(s.sleepTime))
                    print("#     d. bufferTime     - " + str(s.bufferTime))
                    print("#     e. difference     - " + str(s.difference))
                    print("#     f. legRaised      - " + str(s.legRaised))
                    print("#     g. currDifference - " + str(s.currDifference))
                    print("#     h. heightUp       - " + str(s.heightUp))
                    print("#     i. heightDown     - " + str(s.heightDown))
                    print("#     j. w              - " + str(s.stance.w))
                    print("#     k. minSpeed       - " + str(s.minSpeed))
                    print("#     l. maxSpeed       - " + str(s.maxSpeed))
                    print("#     m. liftHeight     - " + str(s.stance.liftHeight))
                    print("# ")

                    choice = raw_input("# Make a selection or hit any other key to return: ")
                    if choice == "a": s.maxDifference = int(raw_input("# maxDifference - "))
                    elif choice == "b": s.stepSize = int(raw_input("# stepSize - "))
                    elif choice == "c": s.sleepTime = float(raw_input("# sleepTime  - "))
                    elif choice == "d": s.bufferTime = float(raw_input("# bufferTime - "))
                    elif choice == "e":
                        index = raw_input("# Enter index to modify: ")
                        if(index.isdigit() and int(index) < len(s.difference)):
                            index = int(index)
                            s.difference[index] = int(raw_input("# difference[" + str(index) + "] - "))
                    elif choice == "f":
                        index = raw_input("# Enter index to modify: ")
                        if(index.isdigit() and int(index) < len(s.difference)):
                            index = int(index)
                            s.legRaised[index] = int(raw_input("# legRaised[" + str(index) + "] - "))
                    elif choice == "g":
                        index = raw_input("# Enter index to modify: ")
                        if(index.isdigit() and int(index) < len(s.difference)):
                            index = int(index)
                            s.currDifference[index] = int(raw_input("# currDifference[" + str(index) + "] - "))
                    elif choice == "h": s.heightUp = int(raw_input("# heightUp - "))
                    elif choice == "i": s.heightDown = int(raw_input("# heightDown - "))
                    elif choice == "j": s.stance.w = int(raw_input("# w - "))
                    elif choice == "k": s.minSpeed = int(raw_input("# minSpeed - "))
                    elif choice == "l": s.maxSpeed = int(raw_input("# maxSpeed - "))
                    elif choice == "m": s.stance.liftHeight = int(raw_input("# liftHeight - "))
                    else: return
                    print("")
                    s.stickMove("settings")
                else: print("Invalid command.")

        def stickMove2(s, x, y):
            if s.controlScheme:
                if(x<256 and y>256 and y<768):
                    #ga links
                    print("crab left")
                    s.crabWalk()
                elif(x>768 and y>256 and y<768):
                    #ga rechts
                    print("crab right")
                    s.crabWalk() #Moet nog aangepast om naar rechts te kunnen
                elif(y<256 and x>256 and x<768):
                    #draai rechts
                    print("crab turn right")
                    s.sharpTurn("right")
                elif(y>768 and x>256 and x<768):
                    #draai links
                    print("crab turn left")
                    s.sharpTurn("left")
            else:
                print(str(x) + " | " + str(y))
                s.calcDiff(x)
                if s.backStep:
                    s.sharpTurn(s.innerLegs)
                else:
                    s.moveRipple()

        def moveRipple(s, direction): #direction is "forward" of "backward"
            #sleepTime = 0.01
            #bufferCycles = 0.5 / sleepTime #Eerste waarde is beoogde beweegtijd in seconden

            # if direction == "forward":
            #     placeholder = s.difference
            #     s.difference = s.currDifference

            if direction == "forward":
                s.leftLegs = [1,3,5]
                s.rightLegs = [0,2,4]
            elif direction == "backward":
                s.leftLegs = [0,2,4]
                s.rightLegs = [1,3,5]

            for i in range(0, s.maxDifference, s.stepSize): #Hier hoeft niet per se gelopen te worden tot maxDifference. Een kleinere waarde op die plek = een kortere moveRipple call
                for j in range(0,6):
                    if j not in s.deactivatedLegs:
                        if(s.currDifference[j] < s.difference[j] and i > (s.maxDifference/2 - s.difference[j]/2)):
                            #Loop over de grond
                            s.calcLeg(j, s.currDifference[j])
                            s.currDifference[j] += s.stepSize
                        elif(s.currDifference[j] >= s.difference[j]):
                            #Reset leg
                            if(s.legRaised[j] < s.bufferCycles):
                                #Raise leg
                                s.raiseLeg(j)
                                s.legRaised[j] += 1
                            elif(s.legRaised[j] < s.bufferCycles*3):
                                #Forward leg
                                s.forwardLeg(j)
                                s.legRaised[j] += 1
                            elif(s.legRaised[j] < s.bufferCycles*4):
                                #Forward leg
                                s.lowerLeg(j)
                                s.legRaised[j] += 1
                            if(s.legRaised[j] >= s.bufferCycles*4):
                                s.legRaised[j] = 0
                                s.currDifference[j] = 0
                        #if(s.currDifference[j] >= s.bufferDifference[j]):
                        #    s.currDifference[j] = 0
                time.sleep(s.sleepTime)
                s.moveMultiple()

        def testCrab(s):
            wNear = 60
            wFar = 200
            s.turnStepCount = s.maxDifference/2
            for i in range(wNear, wFar, 3):#i gaat naar far, far-(i-near) gaat naar near

                s.highTripod = 'B'
                s.lowTripod = 'A'
                s.stance.setW(i)
                s.calculate(0, s.mid[0], s.heightDown)#B
                s.calculate(1, s.mid[1], s.heightUp)#C
                s.calculate(4, s.mid[4], s.heightDown)#B
                s.calculate(5, s.mid[5], s.heightUp)#C

                s.stance.setW(wFar - (i-wNear))
                s.calculate(2, s.mid[2], s.heightUp)#A
                s.calculate(3, s.mid[3], s.heightDown)#D

                time.sleep(0.01)
                s.moveMultiple()
            for i in range(wNear, wFar, 3):
                s.highTripod = 'A'
                s.lowTripod = 'B'
                s.stance.setW(wFar - (i-wNear))
                s.calculate(0, s.mid[0], s.heightUp)#A
                s.calculate(1, s.mid[1], s.heightDown)#D
                s.calculate(4, s.mid[4], s.heightUp)#A
                s.calculate(5, s.mid[5], s.heightDown)#D

                s.stance.setW(i)
                s.calculate(2, s.mid[2], s.heightDown)#B
                s.calculate(3, s.mid[3], s.heightUp)#C
                time.sleep(0.01)
                s.moveMultiple()
            s.stance.setW(s.w)

        def crabWalk2(s):
            wNear = 60
            wFar = 80
            wPlaceholder = s.w
            s.currDifference = s.maxDifference/2

            #lift high tripod
            for i in s.tripod[s.highTripod]:
                if i in s.leftLegs:
                    s.stance.setW(wNear)
                    s.calculate(i, s.mid[i], s.heightUp)
                elif i in s.rightLegs:
                    s.stance.setW(wFar)
                    s.calculate(i, s.mid[i], s.heightUp)
            #wait
            s.moveMultiple()
            time.sleep(s.sleepTime)
            #forward high tripod
            for i in s.tripod[s.highTripod]:
                if i in s.leftLegs:
                    s.stance.setW(wFar)
                    s.calculate(i, s.mid[i], s.heightUp)
                elif i in s.rightLegs:
                    s.stance.setW(wNear)
                    s.calculate(i, s.mid[i], s.heightUp)

            #drag low tripod
            for i in s.tripod[s.lowTripod]:
                if i in s.leftLegs:
                    s.stance.setW(wNear)
                    s.calculate(i, s.mid[i], s.heightDown)
                elif i in s.rightLegs:
                    s.stance.setW(wFar)
                    s.calculate(i, s.mid[i], s.heightDown)

            s.moveMultiple()
            time.sleep(s.sleepTime*10)

            #lower high tripod
            for i in s.tripod[s.lowTripod]:
                if i in s.leftLegs:
                    s.stance.setW(wFar)
                    s.calculate(i, s.mid[i], s.heightDown)
                elif i in s.rightLegs:
                    s.stance.setW(wNear)
                    s.calculate(i, s.mid[i], s.heightDown)

            #switch high/low designations
            if(s.lowTripod == 'A'):
                s.lowTripod = 'B'
                s.highTripod = 'A'
            elif(s.lowTripod == 'B'):
                s.lowTripod = 'A'
                s.highTripod = 'B'

        def crabWalk(s, direction):
            wNear = 30
            wFar = 35
            steps = 20 #hoeveelheid stapjes waarin de beweging uitgevoerd wordt
            interval = (wFar - wNear)/steps
            wPlaceholder = s.w
            s.currDifference = s.maxDifference/2
            if direction == "left":
                #lift high tripod
                for i in s.tripod[s.highTripod]:
                    if i in s.leftLegs:
                        s.stance.crabCalc(i, wNear, s.heightUp)
                    elif i in s.rightLegs:
                        s.stance.crabCalc(i, wFar, s.heightUp)
                s.moveMultiple()
                time.sleep(s.sleepTime)
                #forward high tripod
                for i in s.tripod[s.highTripod]:
                    if i in s.leftLegs:
                        s.stance.crabCalc(i, wFar, s.heightUp)
                    elif i in s.rightLegs:
                        s.stance.crabCalc(i, wNear, s.heightUp)

                #drag low tripod
                for i in range(0,steps):
                    for i in s.tripod[s.lowTripod]:
                        if i in s.leftLegs:
                            s.stance.crabCalc(i, wFar - (interval*i), s.heightDown)
                        elif i in s.rightLegs:
                            s.stance.calculate(i, wNear + (interval*i), s.heightDown)
                    s.moveMultiple()
                    time.sleep(s.sleepTime)

                #lower high tripod
                for i in s.tripod[s.highTripod]:
                    if i in s.leftLegs:
                        s.stance.setW(wFar)
                        s.calculate(i, s.mid[i], s.heightDown)
                    elif i in s.rightLegs:
                        s.stance.setW(wNear)
                        s.calculate(i, s.mid[i], s.heightDown)
                s.moveMultiple()
                time.sleep(s.sleepTime)

            elif direction == "right":
                #lift high tripod
                for i in s.tripod[s.highTripod]:
                    if i in s.leftLegs:
                        s.stance.crabCalc(i, wFar, s.heightUp)
                    elif i in s.rightLegs:
                        s.stance.crabCalc(i, wNear, s.heightUp)
                s.moveMultiple()
                time.sleep(s.sleepTime)
                #forward high tripod
                for i in s.tripod[s.highTripod]:
                    if i in s.leftLegs:
                        s.stance.crabCalc(i, wNear, s.heightUp)
                    elif i in s.rightLegs:
                        s.stance.crabCalc(i, wFar, s.heightUp)

                #drag low tripod
                for i in range(0,steps):
                    for i in s.tripod[s.lowTripod]:
                        if i in s.leftLegs:
                            s.stance.crabCalc(i, wNear + (interval*i), s.heightDown)
                        elif i in s.rightLegs:
                            s.stance.calculate(i, wFar - (interval*i), s.heightDown)
                    s.moveMultiple()
                    time.sleep(s.sleepTime)

                #lower high tripod
                for i in s.tripod[s.highTripod]:
                    if i in s.leftLegs:
                        s.stance.setW(wNear)
                        s.calculate(i, s.mid[i], s.heightDown)
                    elif i in s.rightLegs:
                        s.stance.setW(wFar)
                        s.calculate(i, s.mid[i], s.heightDown)
                s.moveMultiple()
                time.sleep(s.sleepTime)

                        #switch high/low designations
            if(s.lowTripod == 'A'):
                s.lowTripod = 'B'
                s.highTripod = 'A'
            elif(s.lowTripod == 'B'):
                s.lowTripod = 'A'
                s.highTripod = 'B'

        def sharpTurn(s, direction):
            sleepTime = 0.01 #seconden


            s.stance.setSpeed(400)
            if direction == "left":
                front = s.upper
                back = s.lower
            elif direction == "right":
                front = s.lower
                back = s.upper

            for k in s.tripod[s.highTripod]:#Raise
                s.calculate(k, back[k], s.heightUp)
            s.moveMultiple()
            time.sleep(sleepTime)
            for k in s.tripod[s.highTripod]:#Forward
                s.calculate(k, front[k], s.heightUp)

            if direction == "left":
                for i in range(s.turnStepCount, s.maxDifference, s.stepSize): #Beweeg de lopende tripod volgens kinematica naar achteren
                    for k in s.tripod[s.lowTripod]:
                        s.calculate(k, front[k] - i, s.heightDown)
                    s.moveMultiple()
                    s.turnStepCount += s.stepSize
                    time.sleep(sleepTime)

            if direction == "right":
                for i in range(s.turnStepCount, s.maxDifference, s.stepSize): #Beweeg de lopende tripod volgens kinematica naar achteren
                    for k in s.tripod[s.lowTripod]:
                        s.calculate(k, front[k] + s.turnStepCount, s.heightDown)
                    s.moveMultiple()
                    s.turnStepCount += s.stepSize
                    time.sleep(sleepTime)
            s.turnStepCount = 0

            for k in s.tripod[s.highTripod]: #Lower
                s.calculate(k, front[k], s.heightDown)
            s.moveMultiple()
            time.sleep(sleepTime)
            # Verwissel de tripod-designaties voor de volgende stap
            if(s.lowTripod == 'A'):
                s.lowTripod = 'B'
                s.highTripod = 'A'
            elif(s.lowTripod == 'B'):
                s.lowTripod = 'A'
                s.highTripod = 'B'

        def calcDiff(s, stickPosX): #stickPosX is een waarde van 0 tot 1023. 0 is scherp naar links, 512 is recht vooruit en 1023 is scherp naar rechts.
                stickPosX = stickPosX*10
                diffOffset = s.maxDifference/2
                s.backStep = False
                if(stickPosX <121):
                        s.stepDifference = (diffOffset/121)*stickPosX - diffOffset
                        #Let op: waarde 121 volgt uit snijpunt met x as uit vorige formule -> schrijf daar nog formule voor
                        s.backStep = True
                        s.innerLegs = "left"
                        #Call sharpTurn("left")
                elif(stickPosX <= 200):
                        i = abs(-0.9*stickPosX + 30)
                        s.backStep = False
                        s.innerLegs = "left"
                elif(stickPosX <= 300):
                        i = abs(-1.5*stickPosX + 150)
                        s.backStep = False
                        s.innerLegs = "left"
                elif(stickPosX <= 400):
                        i = abs(-4*stickPosX + 900)
                        s.backStep = False
                        s.innerLegs = "left"
                elif(stickPosX <= 507):
                        i = abs(-100*stickPosX + 39300)
                        s.backStep = False
                        s.innerLegs = "left"
                elif(stickPosX <= 515):
                        #vooruitgaande beweging
                        # for i in range(0,6):
                        #     s.difference[i] = maxDifference
                        s.backStep = False
                        s.innerLegs = "none"
                elif(stickPosX <= 623):
                        i = abs(-100 * stickPosX + 63000)
                        s.backStep = False
                        s.innerLegs = "right"
                elif(stickPosX <= 723):
                        i = abs(-4 * stickPosX + 3192)
                        s.backStep = False
                        s.innerLegs = "right"
                elif(stickPosX <= 823):
                        i = abs(-1.5*stickPosX + 1384.5)
                        s.backStep = False
                        s.innerLegs = "right"
                elif(stickPosX <= 902):
                        i = abs(-0.9*stickPosX + 890.7)
                        s.backStep = False
                        s.innerLegs = "right"
                elif(stickPosX <= 1023):
                        s.stepDifference = (-diffOffset/121)*902+((diffOffset*902)/121)
                        #Let op: waarde 902 volgt uit snijpunt met x as uit vorige formule -> schrijf daar nog formule voor
                        s.backStep = True
                        s.innerLegs = "right"
                        #call sharpTurn("right")

                if(not s.backStep and not s.innerLegs == "none"):
                        #i is de lengte tot het denkbeeldige draaipunt
                        stepInMm = 2*math.tan((diffOffset * 300 * math.pi)/ (1024*180))* s.w

                        innerStepLengthMid = (stepInMm *(i - s.w - 78.92))/(i + s.w + 78.92)          #78.92 is de afstand van het zwaarte punt tot de alpha servo van de middelste poten
                        innerStepLengthOuter = (stepInMm *(i - s.w - 50.6))/(i + s.w + 78.92)         #Afstand van het zwaarte punt tot de alpha servo van de buitenste poten
                        if(innerStepLengthMid == 0):
                                s.stepDifferenceMid = 0
                                s.stepDifferenceOuter = 0
                        else:
                                s.stepDifference =((1024*180)/(300*math.pi))*(math.atan(innerStepLengthMid/(2*s.w)))

                        if s.innerLegs == "left":
                            for i in s.leftLegs:
                                s.difference[i] = int(s.stepDifference)
                        elif s.innerLegs == "right":
                            for i in s.rightLegs:
                                s.difference[i] = int(s.stepDifference)
                s.move("forward")
                for i in range(0,6):
                    s.difference[i] = s.maxDifference
                    s.stepDifference = s.maxDifference
                # placeholderForward = s.forward
                # placeholderBackward = s.backward
                # if(s.innerLegs == "left"):
                #         for i in range(0,6):
                #                 if(i in s.leftLegs):
                #                         s.forward[i] = s.mid[i] - int(s.stepDifference)
                #                         s.backward[i] = s.mid[i] + int(s.stepDifference)
                #         s.move("forward")
                # elif(s.innerLegs == "right"):
                #         for i in range(0,6):
                #                 if(i in s.leftLegs):
                #                         s.forward[i] = s.mid[i] + int(s.stepDifference)
                #                         s.backward[i] = s.mid[i] - int(s.stepDifference)
                #         s.move("forward")
                # else:
                #         s.move("forward")
                # s.forward = placeholderForward
                # s.backward = placeholderBackward


        def pointLeg(self, pootnr, x, y, z):#WORK IN PROGRESS
            #x is w
            #y is height
            #z is alpha
            s.deactivatedLegs = [pootnr]
            wPlaceholder = s.w

        def moveMultiple(s):
            s.stance.moveMultiple()

        def turn(s, direction):
                sleepTime = 0.05
                if(low == 'A'):
                        high == 'B'
                elif(low == 'B'):
                        high == 'A'

                s.raiseLeg(s.tripod[high][0]) # Beweeg de niet-lopende tripod omhoog
                s.raiseLeg(s.tripod[high][1])
                s.raiseLeg(s.tripod[high][2])
                moveMultiple()
                time.sleep(sleepTime)

                forwardLeg(s.tripod[high][0]) # Beweeg de niet-lopende tripod vooruit
                forwardLeg(s.tripod[high][1])
                forwardLeg(s.tripod[high][2])
                for i in range(0, s.difference, s.stepSize): #Difference is de standaard stapgrootte, voor draaien kan dit miss nog wel groter dan voor lopen
                        if(direction == "left"):
                                for j in s.tripod[low]:
                                        calculate(j, s.lower[j] + i, s.heightDown)
                        if(direction == "right"):
                                for j in s.tripod[low]:
                                        calculate(j, s.upper[j] - i, s.heightDown)

                        moveMultiple()
                        time.sleep(sleepTime)
                lowerLeg(s.tripod[high][0])# Laat de niet-lopende tripod zakken
                lowerLeg(s.tripod[high][1])# Nu staan alle poten aan de grond
                lowerLeg(s.tripod[high][2])
                moveMultiple()
                time.sleep(sleepTime)
                if(low == 'A'):
                        low = 'B'
                        high = 'A'
                elif(low == 'B'):
                        low = 'A'
                        high = 'B'


        #-------------------------------#
        #       TWISTER & THREATEN      #
        #-------------------------------#


        def calcTwisterLeg(s, pootnr, height):
                s.calculate(pootnr, s.mid[pootnr], height)

        def calcTwisterHeight(s, x):
                height = s.minTwistHeight + (s.diffTwistHeight * (((math.cos(x-math.pi))/2)+0.5))
                return height

        def twister(s):
                for i in range(0,360):
                        s.twister2(i)

        def twister2(s, input):
                for j in range(0,6):
                        s.relativeAngle = math.radians(s.stickAngle[j] - input)
                        #Nieuwe code
                        if s.stickAngle[j] - input > 160 and s.stickAngle[j] - input < 200:#flip poot omhoog
                            s.stance.gammas[j] = 800
                        else: #beweeg gewoon
                        #/Nieuwe code
                            height = s.calcTwisterHeight(s.relativeAngle)
                            s.calcTwisterLeg(j, height)
                s.moveMultiple()
                time.sleep(0.001)

        def threaten(s, height):
                calcLeg(0, 72, False)
                calcLeg(1, 72, False)
                calcLeg(4, 72, False)
                calcLeg(5, 72, False)

                w
                placeholder = s.w
                s.w = 200

                calculate(2, mid[2], -150 + height)
                calculate(3, mid[3], -150 + height)
                s.w = placeholder
                moveMultiple()
                x.sendData()
                time.sleep(0.001)

        #-------------------------------#
        #	YAW, PITCH, ROLL	#
        #-------------------------------#

        def yaw(s, value):#Positieve waarden draait naar links. Negatieve waarden draait naar rechts.
            oldw = s.w
            s.w -= value
            calculate(0, s.alphas[0], s.legHeight[0])
            calculate(5, s.alphas[5], s.legHeight[5])
            s.w += value*2
            calculate(1, s.alphas[1], s.legHeight[1])
            calculate(4, s.alphas[4], s.legHeight[4])
            moveMultiple()
            s.w = oldw

        def pitch(s, value):#positieve waarden draaien naar beneden, negatieve naar boven.
            for i in [0,1]:
                calculate(i, s.alphas[i], s.legHeight[i]-value)
            for i in [4,5]:
                calculate(i, s.alphas[i], s.legHeight[i]+value)
            moveMultiple()
            for i in [0,1]:#Reset de servo-orientaties
                calculate(i, s.alphas[i], s.legHeight[i]+value)
            for i in [4,5]:
                calculate(i, s.alphas[i], s.legHeight[i]-value)

        def roll(s, value): #Positieve waarden draaien links naar beneden, negatieve rechts naar beneden
            for i in s.leftLegs:
                calculate(i, s.alphas[i], s.legHeight[i]-value)
            for i in s.leftLegs:
                calculate(i, s.alphas[i], s.legHeight[i]+value)
            moveMultiple()
            for i in s.leftLegs:#Reset de servo-orientaties
                calculate(i, s.alphas[i], s.legHeight[i]+value)
            for i in s.leftLegs:
                calculate(i, s.alphas[i], s.legHeight[i]-value)

        #-------------------------------#
        #       WIGGLES 		#
        #-------------------------------#
        def wiggleA(s):#130BPM
                j = 0
                jReachedTop = False
                for i in range(-25, 25):
                        if(j<50 and not jReachedTop):
                                j += 2
                        else:
                                jReachedTop = True
                                j -= 2
                        transXYZ(0,i,j)
                        time.sleep(0.00462)
                jReachedTop = False
                for i in range(25, -25, -1):
                        if(j<50 and not jReachedTop):
                                j += 2
                        else:
                                jReachedTop = True
                                j -= 2
                        transXYZ(0,i,j)
                        time.sleep(0.00462)

        def wiggleB(s):
                j = 0
                jReachedTop = False
                for i in range(0, 50):
                        if(j<50 and not jReachedTop):
                                j += 4
                        else:
                                jReachedTop = True
                                j -= 4
                                if(j <= 0):
                                        j = 0
                                        jReachedTop = False
                        transXYZ(0,i,j)
                        time.sleep(0.008)
                jReachedTop = False
                for i in range(50, 0, -1):
                        if(j<50 and not jReachedTop):
                                j += 4
                        else:
                                jReachedTop = True
                                j -= 4
                                if(j <= 0):
                                        j = 0
                                        jReachedTop = False
                        transXYZ(0,i,j)
                        time.sleep(0.008)

        # def main(s):
        #     #s.stance.middle()
        #     while(True):
        #         s.moveRipple()
        #         time.sleep(2)

        #if __name__ == "__main__":
        #    main(s)
#x = Move()