from ax12sender import Ax12sender
import math
import time

# noinspection PyInterpreter
class Stance:


    def __init__(self):
        self.x = Ax12sender()
        self.speed = 800
        self.h = 50

        self.legHeight = {}
        self.legWidth = {}

        self.alphas = [613,409,512,512,409,613]
        self.betas = [0,0,0,0,0,0]
        self.gammas = [0,0,0,0,0,0]

        self.leftLegs = [0,2,4]
        self.rightLegs = [1,3,5]

        self.heightUp = 50	#Standaard-hoogte voor de loopbeweging (Poot omhoog)
        self.heightDown = 90	#Standaard-hoogte voor de loopbeweging (Poot omlaag)
        self.w = 100  #standaard w

        self.legHeight = [self.heightDown,self.heightDown,self.heightDown,self.heightDown,self.heightDown,self.heightDown] #hoogte per poot. Standaardwaarde (Voor de eerste 'calculate') is 'heightDown'
        self.legWidth = [self.w,self.w,self.w,self.w,self.w,self.w] #afstand tot servo alpha per poot. Standaardwaarde is w

        self.mid = [613,409,512,512,409,613]

        self.liftHeight = 120
        self.liftedLegs = 0
        self.lifted = False

    def calculate(self, pootnr, alpha, height):
        self.w # Afstand tot body
        self.h # Hoogte ten opzichte van servo alpha
        a = 50
        b = 82.5
        c = 137
        h = height

        self.legHeight[pootnr] = height
        self.legWidth[pootnr] = self.w

        #Kinematica berekeningen
        alpha1 = float((float(alpha) - self.mid[pootnr]) / 195)
        s = self.w / math.cos(alpha1) - a
        d = math.sqrt((s*s) + (h*h))
        epsilon = math.atan(h/d)
        beta = math.acos(((c*c) - (b*b) - (d*d)) / (-2 * b * d))
        gamma = math.acos(((d*d)-(b*b)-(c*c)) / (-2 * b * c))

        #De berekening is voor links en rechts anders
        if(pootnr%2 ==0): #Poten aan de linkerkant
            self.alphas[pootnr] = int(alpha)
            self.betas[pootnr] = int(round((512-195 * epsilon+195*beta)))
            self.gammas[pootnr] = int(round((468+195 * gamma)))

        else: #Poten aan de rechterkant
            self.alphas[pootnr] = alpha
            self.betas[pootnr] = int(round((512+195 * epsilon-195*beta)))
            self.gammas[pootnr] = int(round((555-195 * gamma)))

    def setWalkHeight(self, up, down):
        self.heightDown = down
        self.heightUp = up

    def setSpeed(s, speed):
        s.speed = speed


    def crabCalc(self, pootnr, w, height):
            c = 137
            b = 82.5
            h = 30#height

            d = math.sqrt((w-50)*(w-50) + h*h)
            beta = math.acos(((c*c) + (b*b) - (d*d))/(2 * c * b))
            gamma = math.acos(((b*b) + (d*d) - (c*c))/(2*b*d))
            epsilon = math.atan(h/d)

            if pootnr in self.leftLegs:
                self.alphas[pootnr] = self.mid[pootnr]
                self.betas[pootnr] = int(round((512-195 * epsilon+195*gamma)))
                self.gammas[pootnr] = int(round((468+195 * beta)))
            elif pootnr in self.rightLegs:
                self.alphas[pootnr] = self.mid[pootnr]
                self.betas[pootnr] = int(round((512+195 * epsilon-195*gamma)))
                self.gammas[pootnr] = int(round((555-195 * beta)))

    def moveMultiple(self):
        servonrs = [1,2,3,11,12,13,21,22,23,31,32,33,41,42,43,51,52,53]
        positions = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        for i in range(0, len(self.alphas)):
            j = len(self.alphas)-i
            positions[i*3] = self.alphas[i]
            positions[i*3+1] = self.betas[i]
            positions[i*3+2] = self.gammas[i]

        self.x.addMultiple(servonrs, positions, self.speed)
        self.x.sendData()

    def prick(self):
        self.betas[0] = 900
        self.gammas[0] = 900
        self.moveMultiple()

    def prick2(self):
        self.alphas[0] = 800

    def setW(self, w):
        self.w = w

    def middle(self):
        placeholder = self.speed
        self.speed = 150
        for i in range(0,6):
            self.calculate(i, self.mid[i], self.heightDown)
        self.speed = placeholder
        self.moveMultiple()

    def oval(self):
        placeholder = self.speed
        self.speed = 150
        for i in range(0,6):
            self.calculate(i, 512, self.heightDown)
        self.speed = placeholder
        self.moveMultiple()

    def init(self): #Not used
        global speed
        placeholder = self.speed
        speed = 150
        for i in range(0,6):
            if(i in tripod['A']):
                self.calculate(i, backward[i], heightUp)
            elif(i in tripod['B']):
                self.calculate(i, forward[i], heightDown)
        speed = placeholder
        self.moveMultiple()

    def hands(self):
        placeholder = self.speed
        self.speed = 50
        #Voorpoten
        self.calculate(0, 512, 120)
        self.calculate(1, 512, 120)

        #Middelpoten
        self.calculate(2, 362, 130)
        self.calculate(3, 662, 130)

        #Achterpoten
        for i in range(4,6):
            self.calculate(i, 512, 80)

        self.speed = placeholder
        self.moveMultiple()
        time.sleep(1)
        self.calculate(0, 512, 180)
        self.calculate(1, 512, 180)
        time.sleep(0.5)
        self.calculate(0, 812, 120)
        self.calculate(1, 212, 120)


    def pushup(self):
        self.calculate(4, self.alphas[4], 150)
        self.calculate(5, self.alphas[5], 150)

        #-------------------------------#
        #       TRANSLATE BODY		#
        #-------------------------------#
        # Voor alle drie translate-methodes is het een goede praktijk om bij gebruik altijd
        # als laatste weer terug te keren naar de oorspronkelijke waarde. Deze zijn als volgt:
        # transX --> w
        # transY --> h
        # transZ --> alpha (De hele array!)
        # Het is dus handig om bij gebruik van deze methodes de oorspronkelijke waardes in een
        # lokale variabele op te slaan.
        # NB: Deze methodes berekenen alleen - moveMultiple moet apart worden aangeroepen om ook iets te bewegen!

        # Deze functie beweegt het body zijwaarts terwijl de poten blijven staan
        # Dit wordt gedaan door de 'w'-waarde van de poten individueel te veranderen,
        # in plaats van deze voor allen gelijk is (Zoals bij een loop-beweging).
        # Bij transValue = 0 blijft de w-waarde gelijk.

        # Deze functie beweegt het body op en neer, terwijl de poten blijven staan.
        # Dit wordt gedaan door de h-waarde te veranderen voor alle poten, in plaats van individueel.
        # Bij transValue = 0 blijft de h-waarde gelijk.
        #ef transY(transValue): #Positief is omhoog, negatief is omlaag

        # Deze functie beweegt het body naar voor en naar achter, terwijl de poten blijven staan.
        # Dit wordt gedaan door de alpha-waarde te veranderen voor alle poten, in plaats van individueel.
        # Bij transValue = 0 blijft de alpha-waarde gelijk.


    def recordPose(s):
            s.recordalphas = s.alphas
            s.recordbetas = s.betas
            s.recordgammas = s.gammas
            s.recordW = s.w

    def resetPose(s):
            s.alphas = records.alphas
            s.betas = records.betas

    def transXYZ(s, x, y, z):
            oldw = s.w
            for i in range(0,6):
                if(s.legHeight[i] + y < 200):
                    if(i in s.leftLegs):
                           s.w = oldw - x
                           s.calculate(i, s.alphas[i] + z, s.legHeight[i] + y)
                    else:
                           s.w = oldw + x
                           s.calculate(i, s.alphas[i] - z, s.legHeight[i] + y)
            s.moveMultiple()
            #Reset waardes
            # for i in range(0,6):
            #         if(i in s.leftLegs):
            #                s.w = oldw - xq
            #                calculate(i, s.alphas[i] - z, s.legHeight[i] - y)
            #         else:
            #                s.w = oldw + x
            #                calculate(i, s.alphas[i] + z, s.legHeight[i] - y)
            #
            # #Reset s.w naar de oude waarde
            # s.w = oldw


    def liftOneLeg(s):
        s.oval()
        if not s.lifted:
            s.calculate(0, s.alphas[0], s.legHeight[0] + s.liftHeight)
        else:
            s.calculate(0, s.alphas[0], s.legHeight[0] - s.liftHeight)

    def twisterLift(s):
        s.oval()
        placeholderW = s.w
        s.w = 125
        stepSize = 4
        for i in range(0, 6):
            for j in range(0, s.liftHeight, stepSize):
                s.legHeight[i] -= stepSize
                print i, s.legHeight[i]
                s.calculate(i, s.alphas[i], s.legHeight[i])
                s.moveMultiple
                time.sleep(0.006)
            for j in range(0, s.liftHeight, stepSize):
                s.legHeight[i] += stepSize
                s.calculate(i, s.alphas[i], s.legHeight[i])
                s.moveMultiple()
                time.sleep(0.006)
        s.w = placeholderW